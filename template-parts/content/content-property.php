<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

	  include_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php';
	  include_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php';
?>

<article id="property-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="property_card">
			<a href="<?php the_permalink();?>" class="link-img-property">
				<?php the_post_thumbnail('350-350'); ?> 
			</a>
			<?php 
				$wish_is_connect = (is_user_logged_in())?'wish_btn_connect':'wish_btn_not_connect';
				$whishlistUser = get_query_var('whishlistUser');
				$like = (is_array($whishlistUser) && in_array(get_the_id(), $whishlistUser)) ? 'like':'';
			?>
			<div class="like_container <?php echo $like;?>">
				
				<svg data-id-property="<?php the_id();?>" class="<?php echo $wish_is_connect;?>" xmlns="http://www.w3.org/2000/svg" width="36.258" height="31.658" viewBox="0 0 36.258 31.658">
					<g id="Tracé_77" data-name="Tracé 77" transform="translate(-243.942 -211.396)" fill="none">
						<path d="M277.085,214.569a10.636,10.636,0,0,0-14.984-.056h0a10.637,10.637,0,0,0-15.043,15.043l12.408,12.408a3.727,3.727,0,0,0,5.271,0l12.408-12.408h0A10.636,10.636,0,0,0,277.085,214.569Z" stroke="none"/>
						<path d="M 254.5785522460938 215.3954925537109 C 252.8056640625 215.3954925537109 251.1389770507812 216.0858459472656 249.8854370117188 217.3394012451172 C 248.6318664550781 218.5929412841797 247.9414978027344 220.2596740722656 247.9414978027344 222.0325469970703 C 247.9414978027344 223.805419921875 248.6318664550781 225.4721527099609 249.8854370117188 226.7257080078125 L 262.1000061035156 238.9402923583984 L 274.2918701171875 226.7457427978516 C 275.5283508300781 225.4904327392578 276.2059936523438 223.8282623291016 276.199951171875 222.0654602050781 C 276.1939697265625 220.3021087646484 275.50390625 218.6442718505859 274.2569274902344 217.3973083496094 C 273.0039367675781 216.1443176269531 271.3377685546875 215.4541778564453 269.5647583007812 215.4540100097656 C 267.8113403320312 215.4540100097656 266.1577453613281 216.1316680908203 264.9085388183594 217.3621368408203 L 262.0689697265625 220.15625 L 259.2669067382812 217.3346099853516 C 258.013916015625 216.0841217041016 256.3491516113281 215.3954925537109 254.5785522460938 215.3954925537109 M 254.5785522460938 211.3954925537109 C 257.3008422851562 211.3954925537109 260.0231628417969 212.4339904785156 262.1001281738281 214.5109710693359 L 262.1015625 214.5124053955078 C 266.2600402832031 210.4163208007812 272.9505615234375 210.4341430664062 277.0853576660156 214.5688781738281 C 281.2198791503906 218.7033843994141 281.2376708984375 225.3942108154297 277.1416015625 229.5527038574219 L 277.1432495117188 229.5541229248047 L 264.7354736328125 241.9616851806641 C 263.2798767089844 243.417236328125 260.9200744628906 243.4171752929688 259.4645385742188 241.9616851806641 L 247.0570068359375 229.5541229248047 C 242.9030151367188 225.400146484375 242.9030151367188 218.6649475097656 247.0570068359375 214.5109710693359 C 249.1339721679688 212.4339904785156 251.8562622070312 211.3954925537109 254.5785522460938 211.3954925537109 Z" stroke="none" fill="#fff"/>
					</g>
				</svg>

			</div>

			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>

			<p class="property_city">
				<?php the_field('city', get_the_id()); ?>
			</p>

			<div class="property_services">
				<?php
					$terms = get_the_terms(get_the_id(), "service");
					if(isset($terms[0])){
						foreach($terms as $term){
							$image = get_field('image', 'service_'.$term->term_id);
							
							if( $image ) {
								?>
									<img src="<?php echo $image;?>" class="svg" width="16px" height="auto" alt="<?php echo $term->name;?>">
								<?php
								/*$upload_dir = wp_upload_dir();
								$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
								$wp_filesystem = new WP_Filesystem_Direct(null);
								$svg = $wp_filesystem->get_contents($svgpath);
								echo $svg;*/
							}
						}
					}
				?>
			</div>

			<p class="property_price">
				<?php
				if(get_field('price',get_the_id()) > 0):
					echo "À partir de ".get_field('price',get_the_id())." €";
				else:
					echo 'Gratuit';
				endif;
				?>	
			</p>
			<?php if(get_field('price',get_the_id()) > 0):?>
			<p class="property_during">
				<?php				
					$during = get_field('during',get_the_id()); 
					if($during == 'hour'){$during = "L’heure";}
					else if($during == 'hours'){$during = "Les deux heures";}
					else if($during == 'half-day'){$during = "La demi-journée";}
					else if($during == 'day'){$during = "La journée";}
					else if($during == 'week'){$during = "La semaine";}
					echo $during;
				?>
			</p>
			<?php endif;?>
			<a href="<?php the_permalink();?>" class="button"><?php _e('Voir cet espace','aosc');?></a>
		</div>

</article><!-- #post-<?php the_ID(); ?> -->
