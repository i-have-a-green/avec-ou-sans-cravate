<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
$comment = get_query_var('comment');

?>

<div class="last-ask last-history custom-roundblock">
    <div class="cols">
        <div class="col">
            <h3><?php echo get_the_title($comment->comment_post_ID);?></h3>
            <hr class="wp-block-separator has-text-color has-background has-orange-background-color has-orange-color">
            <p class="h4">Demande pour le&nbsp;: <span><?php echo date_i18n( get_option( 'date_format' ), strtotime( get_comment_meta( $comment->comment_ID, 'ask_date', true ) ) ); ?></span></p>
            <p class="h4">Demande émise le&nbsp;: <span><?php echo date_i18n( get_option( 'date_format' ), strtotime($comment->comment_date ) ); ?></span></p>
        </div>
        <div class="col">
            <div class="custom-grid">
                <?php 
                $user = wp_get_current_user();
		        $user_roles = $user->roles;
		        if ( in_array( 'coworker', $user_roles, true )):
                    $post_id = $comment->comment_post_ID;
                    $author_id = get_post_field('post_author', $post_id);
                ?>
                    <p class="h4">Prénom&nbsp;Nom&nbsp;:</p><p><?php echo get_the_author_meta( 'display_name' , $author_id );?></p>
                    <p class="h4">Adresse&nbsp;mail&nbsp;:</p><p><?php echo get_the_author_meta( 'user_email' , $author_id );?></p>
                    <p class="h4">Téléphone&nbsp;:</p><p><?php echo get_the_author_meta( 'user_phone' , $author_id ); ?></p>
                <?php else:?>
                    <p class="h4">Prénom&nbsp;Nom&nbsp;:</p><p><?php echo $comment->comment_author;?></p>
                    <p class="h4">Adresse&nbsp;mail&nbsp;:</p><p><?php echo $comment->comment_author_email;?></p>
                    <p class="h4">Téléphone&nbsp;:</p><p><?php echo get_comment_meta( $comment->comment_ID, 'user_phone', true ); ?></p> 
                <?php endif;
                
                $during = get_comment_meta( $comment->comment_ID, 'date_during', true );
                if($during == 'hour'){$during = "Une heure";}
                else if($during == 'hours'){$during = "Deux heures";}
                else if($during == 'half-day'){$during = "Une demi-journée";}
                else if($during == 'day'){$during = "Une journée";}
                else if($during == 'week'){$during = "Une semaine";}
                ?>
                <p class="h4">Durée&nbsp;:</p><p><?php echo $during;?></p> 
                <p class="h4">Participants&nbsp;:</p><p><?php echo get_comment_meta( $comment->comment_ID, 'ask_participants_number', true ); ?></p>
            </div>
        </div>
        <div class="col">
            <div class="custom-grid">
                <p class="h4">Commentaire&nbsp;:</p><p class="commentary"><?php echo get_comment_meta( $comment->comment_ID, 'ask_comment', true ); ?></p>
            </div>
        </div>
    </div>
</div>