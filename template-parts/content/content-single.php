<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header single-header alignwide">
		<!--<div class="wrapper-single ">
			<?php //the_breadcrumb();
			//	$term = ihag_get_term($post, "category");
			?>
			<a href="<?php echo get_term_link($term);?>">
				<p class="meta-nav">
				<?php 
			//		$twentytwentyone_prev = is_rtl() ? ihag_get_icon_svg( 'ui', 'arrow_right' ) : ihag_get_icon_svg( 'ui', 'arrow_left' );
			//		_e($twentytwentyone_prev . "Retour aux articles" , 'ihag');
				?>					
				</p>

			</a>
			
		</div>-->

		<div class="default-max-width">
			<p class="article-single">
				<span>
					<?php
					_e("Publié le ", 'ihag');
					echo get_the_date();
					?>				
				</span>
				<br>
				<?php
				_e("Par ", 'ihag');
				echo get_the_author();
				?>			
			</p>
			<?php the_title( '<h1 class="entry-title" style="font-size:55px;"><strong>', '</strong></h1>' ); ?>
			<?php //ihag_post_thumbnail(); ?>			
		</div>

	</header><!-- .entry-header -->

	<div class="entry-content alignwide">
		<?php
		the_content();

		/*wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'ihag' ) . '">',
				'after'    => '</nav>',
				translators: %: Page number. 
				'pagelink' => esc_html__( 'Page %', 'ihag' ),
			)
		);*/
		?>
	</div><!-- .entry-content -->

	


</article><!-- #post-<?php the_ID(); ?> -->
