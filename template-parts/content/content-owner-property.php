<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

	  include_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php';
	  include_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php';
?>

<article id="property-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="property_card">
			<?php the_post_thumbnail('350-350'); ?>

			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>

			<p class="property_city">
				<?php the_field('city'); ?>
			</p>

			<div class="property_services">
				<?php
					$terms = get_the_terms($post->ID, "service");
					if(isset($terms[0])){
						foreach($terms as $term){
							$image = get_field('image', 'service_'.$term->term_id);
							
							if( $image ) {
								$upload_dir = wp_upload_dir();
								$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
								$wp_filesystem = new WP_Filesystem_Direct(null);
								$svg = $wp_filesystem->get_contents($svgpath);
								echo $svg;
							}
						}
					}
				?>
			</div>

			<p class="property_price">
				à partir de <?php the_field('price'); ?> €
			</p>

			<p class="property_during">
				<?php 
					$during = get_field('during');
					if($during == 'hour'){$during = "L’heure";}
					else if($during == 'hours'){$during = "Les deux heures";}
					else if($during == 'half-day'){$during = "La demi-journée";}
					else if($during == 'day'){$during = "La journée";}
					else if($during == 'week'){$during = "La semaine";} 
					echo $during;
				?>
			</p>

			<?php
				$args = array(
					'post_id' => get_the_id(), 
					'count'   => true // Return only the count.
				);
				$comments_count = get_comments( $args );
			?>
			<p>
				<b><?php _e('Nombre de contact : ');?></b><?echo $comments_count ;?>
			</p>
			<p>
				<b><?php _e('Nombre de favoris : ');?></b><?echo (int)get_post_meta(get_the_id(), 'count_wish', true) ;?>
			</p>

			<a href="<?php echo get_permalink(get_field('owner-add-property', 'option'));?>?id_property=<?php the_id();?>" class="button"><?php _e('Modifier cet espace','aosc');?></a>
		</div>

</article><!-- #post-<?php the_ID(); ?> -->
