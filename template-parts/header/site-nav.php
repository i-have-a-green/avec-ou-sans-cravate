<?php
/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<?php

if ( is_user_logged_in() ) {
	$user = wp_get_current_user();
	if (in_array( "owner", $user->roles, true )){
		if ( has_nav_menu( 'primary' ) ) : ?>
			<nav id="site-navigation" class="primary-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'ihag' ); ?>">
				<div class="menu-button-container">
					<button id="primary-mobile-menu" class="button" aria-controls="primary-menu-list" aria-expanded="false">
						<span class="dropdown-icon open"><?php esc_html_e( 'Menu', 'ihag' ); ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Noeud-Pap.png" alt="">
						</span>
						<span class="dropdown-icon close"><?php esc_html_e( 'Close', 'ihag' ); ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Noeud-Pap-Close.png" alt="">
						</span>
					</button><!-- #primary-mobile-menu -->
				</div><!-- .menu-button-container -->
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'main-loggedIn-owner',
							'menu_class'      => 'menu-wrapper',
							'container_class' => 'primary-menu-container',
							'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
							'fallback_cb'     => false,
						)
					);
				?>
			</nav><!-- #site-navigation -->
		<?php endif;
	}else{
		if ( has_nav_menu( 'primary' ) ) : ?>
			<nav id="site-navigation" class="primary-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'ihag' ); ?>">
				<div class="menu-button-container">
					<button id="primary-mobile-menu" class="button" aria-controls="primary-menu-list" aria-expanded="false">
						<span class="dropdown-icon open"><?php esc_html_e( 'Menu', 'ihag' ); ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Noeud-Pap.png" alt="">
						</span>
						<span class="dropdown-icon close"><?php esc_html_e( 'Close', 'ihag' ); ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Noeud-Pap-Close.png" alt="">
						</span>
					</button><!-- #primary-mobile-menu -->
				</div><!-- .menu-button-container -->
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'main-loggedIn',
							'menu_class'      => 'menu-wrapper',
							'container_class' => 'primary-menu-container',
							'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
							'fallback_cb'     => false,
						)
					);
				?>
			</nav><!-- #site-navigation -->
		<?php endif;
	}
} else {
	if ( has_nav_menu( 'primary' ) ) : ?>
		<nav id="site-navigation" class="primary-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'ihag' ); ?>">
			<div class="menu-button-container">
				<button id="primary-mobile-menu" class="button" aria-controls="primary-menu-list" aria-expanded="false">
					<span class="dropdown-icon open"><?php esc_html_e( 'Menu', 'ihag' ); ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Noeud-Pap.png" alt="">
					</span>
					<span class="dropdown-icon close"><?php esc_html_e( 'Close', 'ihag' ); ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Noeud-Pap-Close.png" alt="">
					</span>
				</button><!-- #primary-mobile-menu -->
			</div><!-- .menu-button-container -->
			<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'menu_class'      => 'menu-wrapper',
						'container_class' => 'primary-menu-container',
						'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
						'fallback_cb'     => false,
					)
				);
			?>
		</nav><!-- #site-navigation -->
	<?php endif;
}
?>

