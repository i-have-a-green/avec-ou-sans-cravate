<?php
/**
 * Template part pour le menu coworker
 */
?>

<?php wp_nav_menu(
    array(
        'theme_location' => 'coworker-menu',
        'container_class' => 'coworker_menu aligndefault'
    )
); ?>