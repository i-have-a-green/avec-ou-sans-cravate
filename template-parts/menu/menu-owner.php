<?php
/**
 * Template part pour le menu coworker
 */
?>

<?php wp_nav_menu(
    array(
        'theme_location' => 'owner-menu',
        'container_class' => 'owner_menu aligndefault'
    )
); ?>