<?php
/**
* Block Name: home-properties
*/
?>
<div class="property_grid">
    <?php
        $whishlistUser = array(); 
        if(is_user_logged_in()){
            $user = wp_get_current_user();
            $whishlistUser = get_user_meta($user->id, 'wishlist', true);
        }
        $args = array(
            'posts_per_page'    => 3,
            'post_type'         => 'property',
            'orderby'           => 'rand',
            'post_status'       => 'publish',
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
                set_query_var( 'whishlistUser', $whishlistUser );
                get_template_part( 'template-parts/content/content', 'property' ); 
            endwhile;
            wp_reset_postdata();
        endif; 
    ?>
</div>