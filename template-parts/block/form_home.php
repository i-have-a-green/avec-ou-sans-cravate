<?php
/**
 * Block Name: Form Home
 */
?>
<form class="property_search_form" method="post" action="<?php echo get_post_type_archive_link( 'property' ); ?>">
	<div class="inner_form">
		<input type="hidden" name="action" value="search_property">
		<input type="hidden" name="distance" value="10">

		<label for="taxo_type"><?php _e("Type d'espace de travail","aosc");?></label>
		<select name="taxo_type" id="taxo_type">
			<option value=""><?php _e("Que cherchez vous ?","aosc");?></option>
			<?php
			$types = get_terms(array('taxonomy' => 'type'));
			foreach($types as $type):?>
				<option value="<?php echo $type->term_id;?>"><?php echo $type->name;?></option>
			<?php 
			endforeach;
			?>
		</select>
	</div>
	<div class="inner_form">
		<label for="address"><?php _e("Ville","aosc");?></label>
		<input type="text" name="address" id="address" placeholder="<?php _e("Où cherchez vous ?","aosc");?>" value="">
	</div>
	
	<div class="inner_form submit-image">
		<input type="image" alt="Rechercher" src="<?php echo get_template_directory_uri(); ?>/assets/images/Loupe.png" name="clicImage"/>
	</div>
</form>
