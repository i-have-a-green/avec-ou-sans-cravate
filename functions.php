<?php

/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 3);
if (!defined('AUTOSAVE_INTERVAL')) define('AUTOSAVE_INTERVAL', 600);
// Supprime la taxo Tags des Articles
function ihag_custom()
{
	register_taxonomy('post_tag', array("category"));
	remove_post_type_support('page', 'thumbnail');
}
add_action('init', 'ihag_custom');

add_filter('wp_revisions_to_keep', 'filter_function_name', 10, 2);
function filter_function_name($num, $post)
{
	return 3;
}

function rjs_lwp_contactform_css_js()
{
	global $post;
	if (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'contact-form-7')) {
		wp_enqueue_script('contact-form-7');
		wp_enqueue_style('contact-form-7');
	} else {
		wp_dequeue_script('contact-form-7');
		wp_dequeue_style('contact-form-7');
	}
}
add_action('wp_enqueue_scripts', 'rjs_lwp_contactform_css_js');

add_filter('wpseo_json_ld_output', '__return_false');

add_action('customize_register', 'jt_customize_register');
function jt_customize_register($wp_customize)
{

	$wp_customize->remove_control('custom_css');
}

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );
function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	$html = preg_replace('/style="[^\"]*"/', '', $html);
	//$html = preg_replace('/sizes="[^\"]*"/', '', $html);
    return $html;
}


if (!function_exists('ihag_setup')) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @since Twenty Twenty-One 1.0
	 *
	 * @return void
	 */
	function ihag_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Twenty-One, use a find and replace
		 * to change 'ihag' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('ihag', get_theme_file_path() . '/languages');

		// Add default posts and comments RSS feed links to head.
		//add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * This theme does not use a hard-coded <title> tag in the document head,
		 * WordPress will provide it for us.
		 */
		//add_theme_support( 'title-tag' );

		/**
		 * Add post-formats support.
		 */
		/*add_theme_support(
			'post-formats',
			array(
				'small_width',
				'aside',
				'gallery',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);*/

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(1568, 9999);

		/* add_image_size("150-150", 100, 100);
		add_image_size("9999-150", 9999, 150);
		add_image_size("9999-100", 9999, 100); */
		add_image_size("350-350", 400, 275, true);
		/* add_image_size("450-450", 450, 450);
		add_image_size("650-650", 650, 650); */
		add_image_size("750-400", 800, 450, true);
		add_filter('image_size_names_choose', 'ihag_custom_sizes');


		function ihag_custom_sizes($sizes)
		{
			return array_merge($sizes, array(
/* 				'150-150' => __('150 * 150'), */
/* 				'9999-150' => __('9999 * 150'), */
/* 				'9999-100' => __('9999 * 100'), */
				'350-350' => __('350 * 350'),
/* 				'450-450' => __('450 * 450'), */
				'750-400' => __('750 * 400')/* ,
				'650-650' => __('650 * 650') */
			));
		}
		register_nav_menus(
			array(
				'primary' => esc_html__('Menu Header', 'ihag'),
				'footer'  => __('Menu Footer', 'ihag'),
				//'secondary'  => __( 'Menu Header 2', 'ihag' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
				'navigation-widgets',
			)
		);

		/*
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		$logo_width  = 220;
		$logo_height = 43;

		add_theme_support(
			'custom-logo',
			array(
				'height'               => $logo_height,
				'width'                => $logo_width,
				'flex-width'           => true,
				'flex-height'          => true,
				'unlink-homepage-logo' => true,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		// Add support for Block Styles.
		add_theme_support('wp-block-styles');

		// Add support for full and wide align images.
		add_theme_support('align-wide');

		// Add support for editor styles.
		add_theme_support('editor-styles');
		//$background_color = get_theme_mod( 'background_color', 'FFFFFF' );

		/*if ( 127 > ihag_Custom_Colors::get_relative_luminance_from_hex( $background_color ) ) {
			add_theme_support( 'dark-editor-style' );
		}*/

		$editor_stylesheet_path = './assets/css/style-editor.css';

		// Enqueue editor styles.
		add_editor_style($editor_stylesheet_path);

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => esc_html__('Extra small', 'ihag'),
					'shortName' => esc_html_x('XS', 'Font size', 'ihag'),
					'size'      => 12,
					'slug'      => 'extra-small',
				),
				array(
					'name'      => esc_html__('Small', 'ihag'),
					'shortName' => esc_html_x('S', 'Font size', 'ihag'),
					'size'      => 14,
					'slug'      => 'small',
				),
				array(
					'name'      => esc_html__('Normal', 'ihag'),
					'shortName' => esc_html_x('M', 'Font size', 'ihag'),
					'size'      => 16,
					'slug'      => 'normal',
				),
				array(
					'name'      => esc_html__('Large', 'ihag'),
					'shortName' => esc_html_x('L', 'Font size', 'ihag'),
					'size'      => 20,
					'slug'      => 'large',
				),
				array(
					'name'      => esc_html__('Extra large', 'ihag'),
					'shortName' => esc_html_x('XL', 'Font size', 'ihag'),
					'size'      => 26,
					'slug'      => 'extra-large',
				),
				array(
					'name'      => esc_html__('Huge', 'ihag'),
					'shortName' => esc_html_x('XXL', 'Font size', 'ihag'),
					'size'      => 48,
					'slug'      => 'huge',
				),
				array(
					'name'      => esc_html__('Gigantic', 'ihag'),
					'shortName' => esc_html_x('XXXL', 'Font size', 'ihag'),
					'size'      => 96,
					'slug'      => 'gigantic',
				),
			)
		);

		// Custom background color.
		/*add_theme_support(
			'custom-background',
			array(
				'default-color' => 'FFFFFF',
			)
		);*/

		// Editor color palette.
		$black     = '#000000';
		$dark_gray = '#8d8d8d';
		$gray      = '#6f6f6f';
		$gray222   = '#222222';
		$light_gray = '#f9f9f9';
		$green     = '#25bb00';
		$blue      = '#004cb8';
		//$purple    = '#D1D1E4';
		$red       = '#d3000e';
		$orange    = '#EE7104';
		$transparent	= 'transparent';
		$white     = '#FFFFFF';

		/*add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => esc_html__('Gray222', 'ihag'),
					'slug'  => 'gray222',
					'color' => $gray222,
				),
				array(
					'name'  => esc_html__('Black', 'ihag'),
					'slug'  => 'black',
					'color' => $black,
				),
				array(
					'name'  => esc_html__('Dark gray', 'ihag'),
					'slug'  => 'dark-gray',
					'color' => $dark_gray,
				),
				array(
					'name'  => esc_html__('Gray', 'ihag'),
					'slug'  => 'gray',
					'color' => $gray,
				),
				array(
					'name'  => esc_html__('Light gray', 'ihag'),
					'slug'  => 'light-gray',
					'color' => $light_gray,
				),
				array(
					'name'  => esc_html__('Green', 'ihag'),
					'slug'  => 'green',
					'color' => $green,
				),
				array(
					'name'  => esc_html__('Blue', 'ihag'),
					'slug'  => 'blue',
					'color' => $blue,
				),

				array(
					'name'  => esc_html__('Red', 'ihag'),
					'slug'  => 'red',
					'color' => $red,
				),
				array(
					'name'  => esc_html__( 'Orange', 'ihag' ),
					'slug'  => 'orange',
					'color' => $orange,
				),
				array(
					'name'  => esc_html__('Blueishgray', 'ihag'),
					'slug'  => 'blueishgray',
					'color' => $blueishgray,
				),
				array(
					'name'  => esc_html__('White', 'ihag'),
					'slug'  => 'white',
					'color' => $white,
				),
				array(
					'name'  => esc_html__('Transparent', 'ihag'),
					'slug'  => 'transparent',
					'color' => $transparent,
				),
			)
		);*/

		/*add_theme_support(
			'editor-gradient-presets',
			array(
				array(
					'name'     => esc_html__( 'Purple to yellow', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $yellow . ' 100%)',
					'slug'     => 'purple-to-yellow',
				),
				array(
					'name'     => esc_html__( 'Yellow to purple', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $purple . ' 100%)',
					'slug'     => 'yellow-to-purple',
				),
				array(
					'name'     => esc_html__( 'Green to yellow', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $green . ' 0%, ' . $yellow . ' 100%)',
					'slug'     => 'green-to-yellow',
				),
				array(
					'name'     => esc_html__( 'Yellow to green', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $green . ' 100%)',
					'slug'     => 'yellow-to-green',
				),
				array(
					'name'     => esc_html__( 'Red to yellow', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $yellow . ' 100%)',
					'slug'     => 'red-to-yellow',
				),
				array(
					'name'     => esc_html__( 'Yellow to red', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $red . ' 100%)',
					'slug'     => 'yellow-to-red',
				),
				array(
					'name'     => esc_html__( 'Purple to red', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $red . ' 100%)',
					'slug'     => 'purple-to-red',
				),
				array(
					'name'     => esc_html__( 'Red to purple', 'ihag' ),
					'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $purple . ' 100%)',
					'slug'     => 'red-to-purple',
				),
			)
		);*/

		/*
		* Adds starter content to highlight the theme on fresh sites.
		* This is done conditionally to avoid loading the starter content on every
		* page load, as it is a one-off operation only needed once in the customizer.
		*/
		if (is_customize_preview()) {
			require get_theme_file_path() . '/inc/starter-content.php';
			add_theme_support('starter-content', ihag_get_starter_content());
		}

		// Add support for responsive embedded content.
		add_theme_support('responsive-embeds');

		// Add support for custom line height controls.
		add_theme_support('custom-line-height');

		// Add support for experimental link color control.
		add_theme_support('experimental-link-color');

		// Add support for experimental cover block spacing.
		add_theme_support('custom-spacing');

		// Add support for custom units.
		// This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
		add_theme_support('custom-units');
	}
}
add_action('after_setup_theme', 'ihag_setup');

/**
 * Register widget area.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @return void
 */
function ihag_widgets_init()
{

	register_sidebar(
		array(
			'name'          => esc_html__('Footer', 'ihag'),
			'id'            => 'sidebar-footer',
			'description'   => esc_html__('Add widgets here to appear in your footer.', 'ihag'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	/*register_sidebar(
		array(
			'name'          => esc_html__('Pré-footer', 'ihag'),
			'id'            => 'pre-footer',
			'description'   => esc_html__('Add widgets here to appear in your pre-footer.', 'ihag'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);*/
	register_sidebar(
		array(
			'name'          => esc_html__('Header', 'ihag'),
			'id'            => 'sidebar-header',
			'description'   => esc_html__('Add widgets here to appear in your header.', 'ihag'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			/*'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',*/
		)
	);
}
add_action('widgets_init', 'ihag_widgets_init');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @global int $content_width Content width.
 *
 * @return void
 */
function ihag_content_width()
{
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters('ihag_content_width', 750);
}
add_action('after_setup_theme', 'ihag_content_width', 0);

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_scripts()
{
	// Note, the is_IE global variable is defined by WordPress and is used
	// to detect if the current browser is internet explorer.
	/*global $is_IE, $wp_scripts;
	if ( $is_IE ) {
		// If IE 11 or below, use a flattened stylesheet with static values replacing CSS Variables.
		wp_enqueue_style( 'twenty-twenty-one-style', get_template_directory() . '/assets/css/ie.css', array(), wp_get_theme()->get( 'Version' ) );
	} else {*/
	// If not IE, use the standard stylesheet.
	wp_enqueue_style('twenty-twenty-one-style', get_stylesheet_directory_uri() . '/style.css', array(), wp_get_theme()->get('Version'));
	//}

	// RTL styles.
	//wp_style_add_data( 'twenty-twenty-one-style', 'rtl', 'replace' );

	// Print styles.
	wp_enqueue_style('twenty-twenty-one-print-style', get_stylesheet_directory_uri() . '/assets/css/print.css', array(), wp_get_theme()->get('Version'), 'print');

	// Threaded comment reply styles.
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	// Register the IE11 polyfill file.
	/*wp_register_script(
		'twenty-twenty-one-ie11-polyfills-asset',
		get_stylesheet_directory_uri() . '/assets/js/polyfills.js',
		array(),
		wp_get_theme()->get( 'Version' ),
		true
	);*/

	// Register the IE11 polyfill loader.
	/*wp_register_script(
		'twenty-twenty-one-ie11-polyfills',
		null,
		array(),
		wp_get_theme()->get( 'Version' ),
		true
	);
	wp_add_inline_script(
		'twenty-twenty-one-ie11-polyfills',
		wp_get_script_polyfill(
			$wp_scripts,
			array(
				'Element.prototype.matches && Element.prototype.closest && window.NodeList && NodeList.prototype.forEach' => 'twenty-twenty-one-ie11-polyfills-asset',
			)
		)
	);*/

	if(is_single() && 'property' == get_post_type()){

		wp_enqueue_style('slick-css', get_stylesheet_directory_uri() . '/assets/css/slick.css');
		wp_enqueue_script(
			'slick',
			get_stylesheet_directory_uri() . '/assets/js/slick.min.js',
			array("jquery-core")
		);
		
		wp_enqueue_script(
			'slider-product',
			get_stylesheet_directory_uri() . '/assets/js/slider.js',
			array("slick")
		);
	}

	// Main navigation scripts.
	if (has_nav_menu('primary')) {
		wp_enqueue_script(
			'twenty-twenty-one-primary-navigation-script',
			get_stylesheet_directory_uri() . '/assets/js/primary-navigation.js',
			//array( 'twenty-twenty-one-ie11-polyfills' ),
			array(),
			wp_get_theme()->get('Version'),
			true
		);
	}

	// Responsive embeds script.
	/*wp_enqueue_script(
		'twenty-twenty-one-responsive-embeds-script',
		get_stylesheet_directory_uri() . '/assets/js/responsive-embeds.js',
		//array( 'twenty-twenty-one-ie11-polyfills' ),
		array(),
		wp_get_theme()->get('Version'),
		true
	);*/

	wp_enqueue_script(
		'script',
		get_theme_file_uri('/assets/js/script.js'),
		array(),
		wp_get_theme()->get('Version'),
		true
	);
	wp_localize_script('script', 'resturl', site_url() . '/wp-json/aosc/');
	wp_localize_script('script', 'login_url', get_permalink(get_field('connect', 'option')));
	wp_localize_script('script', 'dashboardOwner', get_permalink(get_field('dashboard-owner', 'option')));
	wp_localize_script('script', 'dashboardCoworker', get_permalink(get_field('dashboard-coworker', 'option')));
	wp_localize_script('script', 'thankYouOwner', get_permalink(get_field('thank-you-owner', 'option')));
	wp_localize_script( 'script', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ),
		'nonce' => wp_create_nonce( 'wp_rest' )
	) );

	/*wp_enqueue_script(
		'modale',
		get_theme_file_uri('/assets/js/modale.js'),
		array(),
		wp_get_theme()->get('Version'),
		true
	);*/
	wp_localize_script('script', 'iconBase', get_stylesheet_directory_uri() . '/assets/images/');
}
add_action('wp_enqueue_scripts', 'ihag_scripts');

/**
 * Enqueue block editor script.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_block_editor_script()
{

	wp_enqueue_script('twentytwentyone-editor', get_theme_file_uri('/assets/js/editor.js'), array('wp-blocks', 'wp-dom'), wp_get_theme()->get('Version'), true);
}

add_action('enqueue_block_editor_assets', 'ihag_block_editor_script');

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @link https://git.io/vWdr2
 */
function ihag_skip_link_focus_fix()
{

	// If SCRIPT_DEBUG is defined and true, print the unminified file.
	if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {
		echo '<script>';
		include get_theme_file_path() . '/assets/js/skip-link-focus-fix.js';
		echo '</script>';
	}

	// The following is minified via `npx terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
?>
	<script>
		/(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window.addEventListener("hashchange", (function() {
			var t, e = location.hash.substring(1);
			/^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (/^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus())
		}), !1);
	</script>
<?php
}
add_action('wp_print_footer_scripts', 'ihag_skip_link_focus_fix');

/**
 * Enqueue non-latin language styles.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_non_latin_languages()
{
	$custom_css = ihag_get_non_latin_css('front-end');

	if ($custom_css) {
		wp_add_inline_style('twenty-twenty-one-style', $custom_css);
	}
}
//add_action( 'wp_enqueue_scripts', 'ihag_non_latin_languages' );

// SVG Icons class.
require get_theme_file_path() . '/classes/class-twenty-twenty-one-svg-icons.php';

// Custom color classes.
//require get_theme_file_path() . '/classes/class-twenty-twenty-one-custom-colors.php';
//new ihag_Custom_Colors();

// Enhance the theme by hooking into WordPress.
require get_theme_file_path() . '/inc/template-functions.php';

// Menu functions and filters.
require get_theme_file_path() . '/inc/menu-functions.php';

// Custom template tags for the theme.
require get_theme_file_path() . '/inc/template-tags.php';

// Customizer additions.
require get_theme_file_path() . '/classes/class-twenty-twenty-one-customize.php';
new ihag_Customize();

// Block Patterns.
require get_theme_file_path() . '/inc/block-patterns.php';

// Block Styles.
require get_theme_file_path() . '/inc/block-styles.php';

require get_theme_file_path() . '/inc/acf-block.php';
require get_theme_file_path() . '/inc/custom-post-type.php';

require get_theme_file_path() . '/functions-aosc.php';
require get_theme_file_path() . '/functions-aosc-rest.php';
require get_theme_file_path() . '/functions-aosc-csv.php';

/**
 * Enqueue scripts for the customizer preview.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_customize_preview_init()
{
	wp_enqueue_script(
		'twentytwentyone-customize-helpers',
		get_theme_file_uri('/assets/js/customize-helpers.js'),
		array(),
		wp_get_theme()->get('Version'),
		true
	);
	

	wp_enqueue_script(
		'twentytwentyone-customize-preview',
		get_theme_file_uri('/assets/js/customize-preview.js'),
		array('customize-preview', 'customize-selective-refresh', 'jquery', 'twentytwentyone-customize-helpers'),
		wp_get_theme()->get('Version'),
		true
	);
}
//add_action( 'customize_preview_init', 'ihag_customize_preview_init' );

/**
 * Enqueue scripts for the customizer.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_customize_controls_enqueue_scripts()
{

	wp_enqueue_script(
		'twentytwentyone-customize-helpers',
		get_theme_file_uri('/assets/js/customize-helpers.js'),
		array(),
		wp_get_theme()->get('Version'),
		true
	);
}
add_action('customize_controls_enqueue_scripts', 'ihag_customize_controls_enqueue_scripts');

// Custom Button Style ADD

register_block_style(
	'core/button',
	array(
		'name'  => 'bouton-arrow',
		'label' => __('Bouton borderless', 'wp-rig'),
	)
);


// Remove Span & p & br from CF7
add_filter('wpcf7_autop_or_not', '__return_false');
add_filter('wpcf7_form_elements', function ($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
	$content = str_replace('aria-required="true"', 'aria-required="true" required="required" ', $content);
	return $content;
});

//hidden title 
if (function_exists('wpcf7_add_shortcode')){
	wpcf7_add_shortcode('hidden', 'wpcf7_sourceurl_shortcode_handler', true);
}

function wpcf7_sourceurl_shortcode_handler($tag) {
    //if (!is_array($tag)) return '';

    $name = $tag['name'];
    if (empty($name)) return '';

    $html = '<input type="hidden" name="' . $name . '" value="' . get_the_title() . '" />';
   return $html;
}



add_filter('get_the_archive_title', function ($title) {
	if (is_category()) {
		$title = single_cat_title('', false);
	} elseif (is_tag()) {
		$title = single_tag_title('', false);
	} elseif (is_author()) {
		$title = '<span class="vcard">' . get_the_author() . '</span>';
	} elseif (is_tax()) { //for custom post types
		$title = sprintf(__('%1$s'), single_term_title('', false));
	} elseif (is_post_type_archive()) {
		$title = post_type_archive_title('', false);
	}
	return $title;
});

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point($path)
{
	// update path
	$path = get_stylesheet_directory();
	// return
	return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point($paths)
{
	// remove original path (optional)
	unset($paths[0]);
	// append path
	$paths[] = get_stylesheet_directory();
	// return
	return $paths;
}

function my_acf_init()
{

	//acf_update_setting('google_api_key', 'AIzaSyDFKqR7J4PrQ2JurolHm-CO3CuFrCqmLz4');
}
add_action('acf/init', 'my_acf_init');


// recupere le term(valeur) d'une taxonomy (ex: Brad Pitt est le term de la taxonomy actor)
function ihag_get_term($post, $taxonomy)
{
	if (class_exists('WPSEO_Primary_Term')) :
		$wpseo_primary_term = new WPSEO_Primary_Term($taxonomy, get_the_id($post));
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term = get_term($wpseo_primary_term);
		if (is_wp_error($term)) {
			$term = get_the_terms($post, $taxonomy);
			return $term[0];
		}
		return $term;
	else :
		$term = get_the_terms($post, $taxonomy);
		return $term[0];
	endif;
}

//Video parameters

add_filter('oembed_fetch_url','example_add_vimeo_args',10,3);
function example_add_vimeo_args($provider, $url, $args) {
    
        $args = array(
            'title' => 0,
            'byline' => 0,
            'portrait' => 0,
            'badge' => 0
        );
        $provider = add_query_arg( $args, $provider );
    
    return $provider;   
}


add_filter( 'wpseo_canonical', '__return_false' );