<?php
add_action('rest_api_init', function() {

	register_rest_route( 'aosc', 'ask_property',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_ask_property',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	register_rest_route( 'aosc', 'sign-in-coworker',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_sign_in_coworker',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'aosc', 'owner-profile',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_owner_profile',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	register_rest_route( 'aosc', 'coworker-profile',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_coworker_profile',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'aosc', 'pre-owner',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_pre_owner',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	/*register_rest_route( 'aosc', 'owner-company',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_owner_company',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'aosc', 'coworker-company',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_coworker_company',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);*/

	register_rest_route( 'aosc', 'owner-add-property',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_owner_add_property',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'aosc', 'manage-wishlist',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_manage_wishlist',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	
	/*register_rest_route( 'aosc', 'sos-search',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'aosc_sos_search',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);*/

	
});

function aosc_sign_in_coworker(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {
			// Création de l’utilisateur
			$infos = array(
				'user_login'   	=> sanitize_email($_POST['user_email']),
				'user_email'   	=> sanitize_email($_POST['user_email']),
				'user_pass'    	=> sanitize_text_field($_POST['user_pwd']),
				'role'         	=> 'coworker',
				'display_name' 	=> sanitize_text_field( $_POST['user_email'] ),
				'first_name'	=> sanitize_text_field( $_POST['user_firstname'] ),
				'last_name'	=> sanitize_text_field( $_POST['user_lastname'] ),
			);
			$user_id = wp_insert_user( $infos );
			
			if ( is_wp_error( $user_id ) ) { 
				return new WP_REST_Response( '', 304 );
			}

			$creds = array();
			$creds['user_login']    = sanitize_email($_POST['user_email']);
			$creds['user_password'] = sanitize_text_field($_POST['user_pwd']);
			$user = wp_signon( $creds, false );

			$headers[] = 'From: '.get_bloginfo('name').' <'. get_field('aosc-email', 'option') .'>';
			$to = sanitize_email($_POST['user_email']);
			$subject = __("Votre inscription AvecOuSansCravate a été enregistrée !",'aosc');
			$body = "<p>Félicitation,</p>
			<p>AvecOuSansCravate est heureux de vous compter parmi ses membres.</p>
			<p>Vous pouvez dès maintenant consulter votre espace personnalisé : <b>retrouvez l’historique de vos demandes, vos coups de cœur et ainsi simplifier vos futures recherches…</b></p>
			<p>Vous avez une demande particulière ? Contactez nous par mail : contact@avecousanscravate.com</p>
			<p>L'équipe AOSC</p>";
		
			wp_mail( $to, $subject, $body, $headers);
			$to = get_field('aosc-email', 'option');
			wp_mail( $to, $subject, $body, $headers);

			return new WP_REST_Response( '', 200 );		
		}
	}
	return new WP_REST_Response( '', 304 );		
}

function aosc_ask_property(WP_REST_Request $request){
	
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {

			$ask_time_slot = '';
			if(is_array($_POST['ask_time_slot'])){
				foreach($_POST['ask_time_slot'] as $v){
					$ask_time_slot .= $v.' ';
				}
			}

			$during = '';
			if($_POST['date_during'] == 'hour'){$during = "L’heure";}
			else if($_POST['date_during'] == 'hours'){$during = "Les deux heures";}
			else if($_POST['date_during'] == 'half-day'){$during = "La demi-journée";}
			else if($_POST['date_during'] == 'day'){$during = "La journée";}
			else if($_POST['date_during'] == 'week'){$during = "La semaine";}

			$headers[] = 'From: '.get_bloginfo('name').' <'. get_field('aosc-email', 'option') .'>';

			if(isset($_POST['id_property']) && $_POST['id_property'] != '0'){
				$current_user = wp_get_current_user();
				$data = array(
					'comment_post_ID'      		=> $_POST['id_property'],
					'comment_content'      		=> sanitize_text_field($_POST['ask_comment']),
					'user_id'              		=> $current_user->ID,
					'comment_author'       		=> sanitize_text_field( $_POST['user_firstname'] ).' '.sanitize_text_field( $_POST['user_lastname'] ),
					'comment_author_email' 		=> sanitize_text_field( $_POST['user_email'] ),
					'comment_meta'         		=> array(
						'user_phone' 			=> sanitize_text_field( $_POST['user_phone'] ),
						'ask_date'   		=> sanitize_text_field($_POST['ask_date']),
						'ask_time_slot' 	=> sanitize_text_field($ask_time_slot),
						'date_during'		=> sanitize_text_field($_POST['date_during']),
						'ask_what' 			=> sanitize_text_field($_POST['ask_what']),
						'ask_participants_number' => sanitize_text_field($_POST['ask_participants_number']),
						'ask_comment' 		=> nl2br( sanitize_textarea_field($_POST['ask_comment'])),
					)
				);
		 
				$comment_id = wp_insert_comment( $data );
	
				
				$to = get_the_author_meta( 'email', get_post_field ('post_author', $_POST['id_property']) );

				$subject = "Nouvelle demande depuis AvecOuSansCravate";

				$ask_time_slot = '';
				if(is_array($_POST['ask_time_slot'])){
					foreach($_POST['ask_time_slot'] as $v){
						if($v == "morning"){
							$ask_time_slot .=  __("Matin (8h - 12h)").' ';
						}
						else if($v == "afternoon"){
							$ask_time_slot .=  __("Après-midi (14h - 19h)").' ';
						}
						else if($v == "evening"){
							$ask_time_slot .=  __("Soirée (19h - 22h)").' ';
						}
					}
				}
				
				$body = "	<p>Cher partenaire,</p>
							<p>
								Bonne nouvelle ! <br>Votre offre a tapé dans l'oeil de l'un de nos visiteurs et il souhaite entrer en contact avec vous.<br>
								Vous trouverez ci dessous un recapitulatif de sa demande ainsi que le message qu'il a tenu à vous adresser.</p>
							<p>
								Nom de l'espace : ".get_the_title ( $_POST['id_property'] )."<br>
								Nom : ".sanitize_text_field( $_POST['user_lastname'] )."<br>
								Prénom : ".sanitize_text_field( $_POST['user_firstname'] )."<br>
								Télephone : ".sanitize_text_field( $_POST['user_phone'] )."<br>
								E-mail : ".sanitize_text_field( $_POST['user_email'] )."<br>
								Date : ".date_i18n( get_option( 'date_format' ), strtotime( sanitize_text_field($_POST['ask_date']) ) ) ."<br>
								Créneau : ".$ask_time_slot."<br>
								Durée : ".$during."<br>
								Besoin : ".sanitize_text_field( $_POST['ask_what'] )."<br>
								Participants : ".sanitize_text_field( $_POST['ask_participants_number'] )."<br>
								Commentaires : ".nl2br(stripslashes(sanitize_textarea_field($_POST['ask_comment'])))."<br>
							</p>
							<p>
								Cordialement,<br>
								La team AOSC
							</p>
							";
				wp_mail( $to, $subject, $body, $headers);//mail vers owner
				$to = get_field('aosc-email', 'option');
				wp_mail( $to, $subject, $body, $headers);//mail vers AOSC
				
			}else{
				//DEMANDE SOS
				$body = "	<p>Demande SOS</p>
							<p>
								Nom : ".sanitize_text_field( $_POST['user_lastname'] )."<br>
								Prénom : ".sanitize_text_field( $_POST['user_firstname'] )."<br>
								Télephone : ".sanitize_text_field( $_POST['user_phone'] )."<br>
								E-mail : ".sanitize_text_field( $_POST['user_email'] )."<br>
								Date : ".date_i18n( get_option( 'date_format' ), strtotime( sanitize_text_field($_POST['ask_date']) ) ) ."<br>
								Créneau : ".$ask_time_slot."<br>
								Durée : ".$during."<br>
								Besoin : ".sanitize_text_field( $_POST['ask_what'] )."<br>
								Participants : ".sanitize_text_field( $_POST['ask_participants_number'] )."<br>
								Commentaires : ".nl2br(sanitize_textarea_field($_POST['ask_comment']))."<br>
							</p>
							<p>
								Cordialement,<br>
								La team AOSC
							</p>
							";
				$to = get_field('aosc-email', 'option');
				wp_mail( $to, 'Nouvelle demande SOS depuis AvecOuSansCravate', $body, $headers);
	
			}
			$to = $_POST['user_email'];
			$subject = __("Votre demande depuis le site AvecOuSansCravate",'aosc');
			$body = "	<p>Madame, Monsieur,</p>
						<p>
							Avec ou sans cravate vous remercie pour l'intéret que vous portez à nos services.
						</p>
						<p>
							Votre demande a bien été transmise et nous mettons tout en œuvre pour vous recontacter sous 24h (jour ouvré).
						</p>
						<p>
							Vous avez une question, vous souhaitez un accompagnement sur mesure, vous n'avez pas eu de réponse dans les délais prévus ?
						</p>
						<p>
							Nous sommes joignable au 02.55.65.01.10 ou par courriel : contact@avecousanscravate.com
						</p>
						<p>
							Cordialement,<br>
							La team AOSC
						</p>
						";		
			wp_mail( $to, $subject, $body, $headers);
			return new WP_REST_Response( '', 200 );		
		}    
	}
	return new WP_REST_Response( '', 304 );
}



function aosc_owner_profile(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

			$current_user = wp_get_current_user();
			update_user_meta( $current_user->ID, 'user_lastname',  sanitize_text_field( $_POST['user_lastname'] ) );
			update_user_meta( $current_user->ID, 'user_firstname',  sanitize_text_field( $_POST['user_firstname'] ) );
			update_user_meta( $current_user->ID, 'user_phone',  sanitize_text_field( $_POST['user_phone'] ) );
			update_user_meta( $current_user->ID, 'user_address',  sanitize_text_field( $_POST['user_address'] ) );
			update_user_meta( $current_user->ID, 'linkedin',  sanitize_text_field( $_POST['linkedin'] ) );
			update_user_meta( $current_user->ID, 'user_company',  sanitize_text_field( $_POST['user_company'] ) );
			update_user_meta( $current_user->ID, 'company_activity',  sanitize_text_field( $_POST['company_activity'] ) );
			//update_user_meta( $current_user->ID, 'company_target',  sanitize_text_field( $_POST['company_target'] ) );

			wp_update_user( array( 
				'ID' => $current_user->ID, 
				'user_email' => sanitize_text_field( $_POST['user_email'] ),
				'description' => sanitize_textarea_field( $_POST['company_description'] ) ,
				'last_name' => sanitize_text_field( $_POST['user_lastname'] ),
				'first_name' => sanitize_text_field( $_POST['user_firstname'] ),
			) );

			if(isset($_POST['user_pwd']) && (!empty($_POST['user_pwd']))){
				wp_set_password($_POST['user_pwd'], $current_user->ID);
				$creds = array();
				$creds['user_login']    = $current_user->user_email;
				$creds['user_password'] = $_POST['user_pwd'];
				$user = wp_signon( $creds, false );
			}

			return new WP_REST_Response( '', 200 );
		}
	}
	return new WP_REST_Response( '', 304 );
}

function aosc_coworker_profile(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

			$current_user = wp_get_current_user();
			update_user_meta( $current_user->ID, 'user_lastname',  sanitize_text_field( $_POST['user_lastname'] ) );
			update_user_meta( $current_user->ID, 'user_firstname',  sanitize_text_field( $_POST['user_firstname'] ) );
			update_user_meta( $current_user->ID, 'user_phone',  sanitize_text_field( $_POST['user_phone'] ) );
			update_user_meta( $current_user->ID, 'user_address',  sanitize_text_field( $_POST['user_address'] ) );
			update_user_meta( $current_user->ID, 'linkedin',  sanitize_text_field( $_POST['linkedin'] ) );
			update_user_meta( $current_user->ID, 'user_status',  sanitize_text_field( $_POST['user_status'] ) );
			update_user_meta( $current_user->ID, 'user_company',  sanitize_text_field( $_POST['user_company'] ) );
			update_user_meta( $current_user->ID, 'company_activity',  sanitize_text_field( $_POST['company_activity'] ) );
			//update_user_meta( $current_user->ID, 'company_target',  sanitize_text_field( $_POST['company_target'] ) );
			
			wp_update_user( array( 
				'ID' => $current_user->ID, 
				'user_email' => sanitize_text_field( $_POST['user_email'] ),
				'last_name' => sanitize_text_field( $_POST['user_lastname'] ),
				'first_name' => sanitize_text_field( $_POST['user_firstname'] ),
				//'description' => sanitize_text_field( $_POST['company_description'] ), 
			) );
 

			/*if(isset($_POST['user_pwd']) && (!empty($_POST['user_pwd']))){
				wp_set_password($_POST['user_pwd'], $user_connect->ID);
				$creds = array();
				$creds['user_login']    = $current_user->user_email;
				$creds['user_password'] = $_POST['user_pwd'];
				$user = wp_signon( $creds, false );
			}*/

			return new WP_REST_Response( '', 200 );
		}
	}
	return new WP_REST_Response( '', 304 );
}

function aosc_pre_owner(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {
			// Création de l’utilisateur
			$password = wp_generate_password();
			$infos = array(
				'user_login'   	=> sanitize_email($_POST['user_email']),
				'user_email'   	=> sanitize_email($_POST['user_email']),
				'user_pass'    	=> $password,
				'role'         	=> 'owner', 
				'display_name' 	=> sanitize_text_field( $_POST['user_firstname'] .' '. $_POST['user_lastname'] ),
				'first_name'	=> sanitize_text_field( $_POST['user_firstname'] ),
				'last_name'	=> sanitize_text_field( $_POST['user_lastname'] ),
			);

			$user_id = wp_insert_user( $infos );
			if ( is_wp_error( $user_id ) ) { 
				return new WP_REST_Response( '', 304 );
			}

			$type_property = "";
			if($_POST['type_property']){
				foreach($_POST['type_property'] as $type){
					$type_property .= $type.' - ';
				}
			}

			update_user_meta( $user_id, 'user_company',  sanitize_text_field( $_POST['user_company'] ) );
			update_user_meta( $user_id, 'user_phone',  sanitize_text_field( $_POST['user_phone'] ) );
			update_user_meta( $user_id, 'user_address',  sanitize_text_field( $_POST['user_address'] ) );
			update_user_meta( $user_id, 'type_property',  $type_property  );
			
			

			$headers[] = 'From: '.get_bloginfo('name').' <'. get_field('aosc-email', 'option') .'>';
			$to = sanitize_email($_POST['user_email']);
			$subject = __("Votre inscription AvecOuSansCravate a été enregistrée",'aosc');
			$body = "<p>Félicitation,</p>
			<p>AvecOuSansCravate est heureux de vous compter parmi ses membres. Vous pouvez dès à présent consulter votre espace personnalisé : <b>rédigez votre annonce, ajoutez vos photos, actualisez la disponibilité de vos biens et retrouvez l’historique des demandes…</b></p>
			<p>Vos paramètres de connexion :<br>
				identifiant : ".sanitize_email($_POST['user_email'])."<br>
				Mot de passe : ".$password."<br>
			</p>
			<p>
				N’attendez plus, connectez vous à ".get_permalink( get_field('connect','option') )." et mettez votre bien à disposition de nos visiteurs ! 
			</p>
			<p>Vous avez une demande particulière ? Contactez nous par mail : contact@avecousanscravate.com</p>
			<p>L'équipe AOSC,</p>";
		
			wp_mail( $to, $subject, $body, $headers);



			$to = get_field("aosc-email", "option");
			$subject = __("Demande d'inscription bailleur",'aosc');
			$body = "<p>".sanitize_text_field( $_POST['user_firstname'] .' '. $_POST['user_lastname'] )."</p>
			<p>".sanitize_text_field( $_POST['user_phone'] )."</p>
			<p>".sanitize_text_field( $_POST['user_email'] )."</p>
			<p>".sanitize_text_field( $_POST['user_address'] )."</p>
			<p>".sanitize_text_field( $type_property )."</p>";
		
			wp_mail( $to, $subject, $body, $headers);

			$creds = array();
			$creds['user_login']    = sanitize_email($_POST['user_email']);
			$creds['user_password'] = $password;
			$user = wp_signon( $creds, false );

			return new WP_REST_Response( '', 200 );		
		}
	}
	return new WP_REST_Response( '', 304 );		
}


function aosc_owner_company(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

			/*$current_user = wp_get_current_user();
			update_user_meta( $current_user->ID, 'user_company',  sanitize_text_field( $_POST['user_company'] ) );
			update_user_meta( $current_user->ID, 'company_activity',  sanitize_text_field( $_POST['company_activity'] ) );
			update_user_meta( $current_user->ID, 'company_target',  sanitize_text_field( $_POST['company_target'] ) );
	
			return new WP_REST_Response( '', 200 );*/
			//intégré dans le profile
		}
	}
	return new WP_REST_Response( '', 304 );
}

/*function aosc_coworker_company(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

			$current_user = wp_get_current_user();
			update_user_meta( $current_user->ID, 'user_company',  sanitize_text_field( $_POST['user_company'] ) );
			update_user_meta( $current_user->ID, 'company_activity',  sanitize_text_field( $_POST['company_activity'] ) );
			update_user_meta( $current_user->ID, 'company_target',  sanitize_text_field( $_POST['company_target'] ) );
	
			return new WP_REST_Response( '', 200 );
		}
	}
	return new WP_REST_Response( '', 304 );
}*/

function aosc_owner_add_property(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {
			$post_create = false;
			$current_user = wp_get_current_user();
			if(isset($_POST['id_property']) && $_POST['id_property'] == 0){
				$my_post = array(
					'post_title'    => sanitize_text_field( $_POST['property_name'] ),
					'post_content'  => sanitize_textarea_field($_POST['property_description']),
					'post_status'   => 'draft',
					'post_type'   => 'property',
				);
				
				$post_id = wp_insert_post( $my_post );
				if ( is_wp_error( $post_id ) ) { 
					return new WP_REST_Response( '', 304 );
				}
				$post_create = true;
			}else{
				$post_id = $_POST['id_property'];
				$my_post = array(
					'ID'			=> $post_id,
					'post_title'    => sanitize_text_field( $_POST['property_name'] ),
					'post_content'  => sanitize_textarea_field($_POST['property_description']),
				);
				wp_update_post( $my_post );
			}

			
			update_post_meta($post_id, 'address', sanitize_text_field( $_POST['property_address'] ));
			update_post_meta($post_id, 'zip_code', sanitize_text_field( $_POST['property_postcode'] ));
			update_post_meta($post_id, 'city', sanitize_text_field( $_POST['property_city'] ));
			update_post_meta($post_id, 'access', sanitize_textarea_field( $_POST['access_info'] ));
			update_post_meta($post_id, 'place', sanitize_text_field( $_POST['property_place'] ));
			update_post_meta($post_id, 'area', sanitize_text_field( $_POST['property_area'] ));
			update_post_meta($post_id, 'price', sanitize_text_field( $_POST['property_price'] ));
			update_post_meta($post_id, 'during', sanitize_text_field( $_POST['property_during'] ));
			update_post_meta($post_id, 'leave_date', sanitize_text_field( $_POST['leave_date'] ));

			if(is_array($_POST['taxo_service'])){
				wp_set_object_terms( $post_id, $_POST['taxo_service'], 'service' );
			}
			if(!empty($_POST['property_type'])){
				wp_set_object_terms( $post_id, $_POST['property_type'], 'type' );
			}
			
			
			wp_set_object_terms( $post_id, array(), 'availability' );
			
			if(is_array($_POST['availability'])){
				$availabilities_temp = $_POST['availability'];
				$availabilities = $_POST['availability'];
				foreach($availabilities_temp as $availability){
					$term = get_term_by( 'slug', $availability, 'availability');
					if($term->parent != '0'){
						$term_parent = get_term_by( 'id', $term->parent, 'availability');
						if(!in_array($term_parent->slug, $availabilities)){
							$availabilities[] = $term_parent->slug;
						}
					}
				}
				wp_set_object_terms( $post_id, $availabilities, 'availability' );
			}

			if(isset($_POST['delete_img_principale']) && !empty(($_POST['delete_img_principale']))){
				delete_post_thumbnail($post_id);
			}

			for($img_gallery=1; $img_gallery<=7; $img_gallery++ ){
				if(isset($_POST['delete_img_'.$img_gallery]) && !empty(($_POST['delete_img_'.$img_gallery]))){
					$id_attachment = get_field('img-gallery-'.$img_gallery, $post_id);
					echo ' - '.$id_attachment;
					if($id_attachment){
						wp_delete_attachment($id_attachment);
						delete_field( 'img-gallery-'.$img_gallery, $post_id );
					}
				}
			}

			if(isset($_FILES)){
				if ( ! function_exists( 'wp_crop_image' ) ) {
					include_once( ABSPATH . 'wp-admin/includes/image.php' );
				}
				$upload_dir   = wp_upload_dir();
				$uploaddirimg = $upload_dir['basedir'].'/';
				if(!is_dir($uploaddirimg)){
					mkdir($uploaddirimg, 0755);
				}
				for($img_gallery=1; $img_gallery<=7; $img_gallery++ ){
					if(isset($_FILES['img-gallery-'.$img_gallery])){
						$filename = $uploaddirimg . basename($_FILES['img-gallery-'.$img_gallery]['name']);
						if (move_uploaded_file($_FILES['img-gallery-'.$img_gallery]['tmp_name'], $filename)) {
							$wp_filetype = wp_check_filetype($filename, null );
							$attachment = array(
								'guid'           => $wp_upload_dir['url'] . '/'.basename($_FILES['img-gallery-'.$img_gallery]['name']),
								'post_mime_type' => $wp_filetype['type'],
								'post_parent' => $post_id,
								'post_title' => preg_replace('/\.[^.]+$/', '', basename($_FILES['img-gallery-'.$img_gallery]['name'])),
								'post_content' => '',
								'post_status' => 'inherit'
							);
							$attachment_id = wp_insert_attachment( $attachment, $filename);
			
							$attach_data = wp_generate_attachment_metadata( $attachment_id, $filename );
							wp_update_attachment_metadata( $attachment_id, $attach_data );
							update_field( 'img-gallery-'.$img_gallery, $attachment_id, $post_id );
						}
					}
				}

				if(isset($_FILES['img-gallery-principale'])){
					$filename = $uploaddirimg . basename($_FILES['img-gallery-principale']['name']);
					if (move_uploaded_file($_FILES['img-gallery-principale']['tmp_name'], $filename)) {
						$wp_filetype = wp_check_filetype($filename, null );
						$attachment = array(
							'guid'           => $wp_upload_dir['url'] . '/'.basename($_FILES['img-gallery-principale']['name']),
							'post_mime_type' => $wp_filetype['type'],
							'post_parent' => $post_id,
							'post_title' => preg_replace('/\.[^.]+$/', '', basename($_FILES['img-gallery-principale']['name'])),
							'post_content' => '',
							'post_status' => 'inherit'
						);
						$attachment_id = wp_insert_attachment( $attachment, $filename);
		
						$attach_data = wp_generate_attachment_metadata( $attachment_id, $filename );
						wp_update_attachment_metadata( $attachment_id, $attach_data );
						set_post_thumbnail($post_id, $attachment_id);
					}
				}
			}
			if($post_create){
				$headers[] = 'From: '.get_bloginfo('name').' <'. get_field('aosc-email', 'option') .'>';
				$to = sanitize_email($current_user->user_email);
				$subject = __("Enregistrement d’un nouveau bien",'aosc');
				$body = "<p>Bonjour,</p>
				<p>AvecOuSansCravate a bien pris en compte votre demande d’ajout d’un nouveau bien.</p>
				<p>Nous traitons votre demande rapidement et revenons vers vous avant la publication définitive de la fiche de bien sur notre site. </p>
				<p>Vous avez une demande particulière ? Contactez nous par mail : contact@avecousanscravate.com</p>
				<p>L'équipe AOSC</p>";
			
				wp_mail( $to, $subject, $body, $headers);
		
				
				$subject = __("Enregistrement d’un nouveau bien",'aosc');
				$to = get_field("aosc-email", "option");
				$body = "<p>Bonjour,</p>
				<p>Bonne nouvelle !</p>
				<p>Un hôte déjà inscrit veut enregistrer un nouveau bien :</p>
				<p>". $current_user->user_firstname ." ". $current_user->user_lastname ." ". get_user_meta($current_user->ID,'user_phone',true) ." ". $current_user->user_email ." 
				veut ajouter un nouveau bien nommé : ". sanitize_text_field( $_POST['property_name'] ) ."</p>
				<p>L'équipe AOSC</p>";
			
				wp_mail( $to, $subject, $body, $headers);
			}

			//$post = get_post($post_id);
			//save_property_gps( $post_id, $post, true);

			return new WP_REST_Response( '', 200 );
		}
	}
	return new WP_REST_Response( '', 304 );
}

/*function aosc_sos_search(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

			return new WP_REST_Response( '', 200 );
		}
	}
	return new WP_REST_Response( '', 304 );
}*/

function aosc_manage_wishlist(WP_REST_Request $request){

	if ( check_nonce() ) {
		$user = wp_get_current_user();

		$wishlist = get_user_meta($user->id, 'wishlist', true);
		if(!is_array($wishlist)){
			$wishlist = array();
		}
		$count_wish = (int)get_post_meta($_POST['id_property'], 'count_wish', true);
		if(in_array($_POST['id_property'], $wishlist)){
			foreach($wishlist as $key=>$wish){
				if($wish == $_POST['id_property']){
					unset($wishlist[$key]);
					$count_wish--;
				}
				if(empty($wish)){
					unset($wishlist[$key]);
					$count_wish--;
				}
			}
		}else{
			$wishlist[] = $_POST['id_property'];	
			$count_wish++;
		}
		update_user_meta($user->id, 'wishlist', $wishlist);
		update_post_meta($_POST['id_property'], 'count_wish', $count_wish);
		return new WP_REST_Response( '', 200 );
	}
	return new WP_REST_Response( '', 304 );
}
