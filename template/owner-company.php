<?php
/*
Template Name: owner-company
*/
$user = secure_role('owner');
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="owner-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-owner' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<form action="" method="post"  name="owner-company-form" id="owner-company-form">
		<input type="hidden" name="honeyPot" value="">

		<!--<div class="inner_form">
			<label for="company_name"><?php _e('Nom', 'aosc'); ?></label>
			<input type="text" id="company_name" name="user_company" placeholder="" value="<?php echo get_user_meta($user->ID, 'user_company', true );?>">
		</div>

		<div class="inner_form">
			<label for="company_activity"><?php _e('Activité', 'aosc'); ?></label>
			<input type="text" id="company_activity" name="company_activity" placeholder="" value="<?php echo get_user_meta($user->ID, 'company_activity', true );?>">
		</div>

		<div class="inner_form">
			<label for="company_target"><?php _e('Coeur de cible', 'aosc'); ?></label>
			<input type="text" id="company_target" name="company_target" placeholder="" value="<?php echo get_user_meta($user->ID, 'company_target', true );?>">
		</div>-->

		intégré dans le profile
		
		<div class="btn_container">
			<input type="submit" class="button" id="owner-company-form-btn" value="<?php _e('Valider les informations', 'aosc'); ?>">
		</div>
		<div id="owner-company-form-ok" style="display:none">
			Votre demande a été prise en compte
		</div>

	</form>
</div>

<?php
endwhile; // End of the loop.

get_footer();
