<?php
/*
Template Name: owner-propose-property
*/
$user = secure_role('owner');

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="owner-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-owner' ); ?>
	</div>

	<?php 
	if(!isset($_GET['id_property'])): 
		the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); 
	else: 
		echo '<h2 class="entry-title aligncenter">Modification de '.get_the_title($_GET['id_property']).'</h2>'; 
	endif; ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<form action="" method="post"  name="owner-add-property-form" id="owner-add-property-form">
		<input type="hidden" name="honeyPot" value="">
		<input type="hidden" name="id_property" value="<?php echo (isset($_GET['id_property']))? $_GET['id_property'] :'0';?>">
		
		<div class="inner_form">
			<label for="property_name"><?php _e('Nom de l\'espace *', 'aosc'); ?></label>
			<input type="text" id="property_name" name="property_name" placeholder="Espace de coworking" value="<?php echo (isset($_GET['id_property']))? get_the_title($_GET['id_property']):'';?>" required>
		</div>

		<div class="inner_form">
			<label for="property_address"><?php _e('Adresse de l\'espace *', 'aosc'); ?></label>
			<input type="text" id="property_address" name="property_address" placeholder="Rue du Gois" value="<?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'address', true):'';?>" required>
		</div>
	
		<div class="inner_form">
			<label for="property_postcode"><?php _e('Code postal *', 'aosc'); ?></label>
			<input type="text" id="property_postcode" name="property_postcode" placeholder="85230" value="<?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'zip_code', true):'';?>" required>
		</div>

		<div class="inner_form">
			<label for="property_city"><?php _e('Ville *', 'aosc'); ?></label>
			<input type="text" id="property_city" name="property_city" placeholder="Beauvoir sur mer" value="<?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'city', true):'';?>" required>
		</div>
		<div class="inner_form">
			<label for="property_type"><?php _e('Type d\'espace *', 'aosc'); ?></label>
		
			<select name="property_type" id="property_type">
				<option value=""><?php _e("Sélectionner le type d'espace","aosc");?></option>
				<?php
				$term_type = '';
				if(isset($_GET['id_property'])){
					$terms =  (array)get_the_terms($_GET['id_property'], "type");
					if(sizeof($terms) > 0){
						foreach($terms as $term){
							$term_type = $term->slug;
							break;
						}
					}
				}
				$types = get_terms(array('taxonomy' => 'type', 'hide_empty' => false));
				foreach($types as $type):
					$checked = ($type->slug == $term_type) ? 'checked':'';
				?>
					<option value="<?php echo $type->slug;?>" <?php selected($type->slug, $term_type);?>><?php echo $type->name;?></option>
				<?php 
				endforeach;
				?>
			</select>
		</div>
		<div class="inner_form">
			<label for="property_place"><?php _e('Nombre de place *', 'aosc'); ?></label>
			<input type="number" id="property_place" name="property_place" value="<?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'place', true):'1';?>" required>
		</div>

		<div class="inner_form">
			<label for="property_area"><?php _e('Superficie *', 'aosc'); ?></label>
			<input type="text" id="property_area" name="property_area" value="<?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'area', true):'';?>" placeholder="25m2" >
		</div>

		<div class="inner_form">
			<label for="property_during"><?php _e('Durée de location minimum *', 'aosc'); ?></label>
			<select id="property_during" name="property_during" required>
				<option value="">Choisissez votre durée minumum</option>
				<option value="hour" <?php selected('hour', get_post_meta($_GET['id_property'], 'during', true));?>>Une heure</option>
				<option value="hours" <?php selected('hours', get_post_meta($_GET['id_property'], 'during', true));?>>Deux heures</option>
				<option value="half-day" <?php selected('half-day', get_post_meta($_GET['id_property'], 'during', true));?>>Une demi-journée</option>
				<option value="day" <?php selected('day', get_post_meta($_GET['id_property'], 'during', true));?>>Une journée</option>
				<option value="week" <?php selected('week', get_post_meta($_GET['id_property'], 'during', true));?>>Une semaine</option>
			</select>
		</div>

		<div class="inner_form">
			<label for="property_price"><?php _e('Tarif (à la durée de location séléctionnée)*', 'aosc'); ?><br><small><em>Nos équipes peuvent vous accompagner à déterminer le juste prix</em></small></label>
			<input type="number" min="0" step="0.01" id="property_price" name="property_price" placeholder="10" value="<?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'price', true):'';?>" required>
		</div>
	
		<div class="inner_form">
			<label for="property_availability"><?php _e('Disponibilité du bien :', 'aosc'); ?></label>
			<div class="inner_form checkboxes-grid">
				<?php 
				$terms_tab = array();
				if(isset($_GET['id_property'])){
					$terms =  (array)get_the_terms($_GET['id_property'], "availability");
					if(sizeof($terms) > 0){
						foreach($terms as $term){
							$terms_tab[] = $term->slug;
						}
					}
				}
				$availabilities = get_terms( array( 'taxonomy' => 'availability', 'parent' => 0,'hide_empty' => false ) );
				foreach($availabilities as $availability){
					echo '<div class="inner_form checkboxes">
					<label for="availability_'.$availability->slug.'">'.$availability->name.'</label>
					<div class="checkboxes_custom">';
					$children = get_term_children($availability->term_id, 'availability');
					foreach($children as $child){
						$child = get_term_by( 'id', $child, 'availability');
						$checked = (in_array($child->slug, $terms_tab)) ? 'checked':'';
						echo '<div class="solo_checkbox"><input type="checkbox" name="availability[]" value="'.$child->slug.'" '.$checked.'>'.$child->name.'</div>';
					}
					echo '</div></div>';
				}
				?>			
			</div>
		</div>
		
		<div class="inner_form">
			<label for="taxo_service"><?php _e('Services :', 'aosc'); ?></label>
			<div class="inner_form checkboxes-grid checkboxes">
			<?php
			$terms_tab = array();
			if(isset($_GET['id_property'])){
				$terms = (array)get_the_terms($_GET['id_property'], "service");
				if(sizeof($terms) > 0){
					foreach($terms as $term){
						$terms_tab[] = $term->slug;
					}
				}
			}
			$services = get_terms(array('taxonomy' => 'service', 'hide_empty' => false));
			foreach($services as $service): $checked = (in_array($service->slug, $terms_tab)) ? 'checked':'';?>
				<div class="solo_checkbox"><input type="checkbox" name="taxo_service[]" value="<?php echo $service->slug;?>" <?php echo $checked;?>><?php echo $service->name;?></div>
			<?php 
			endforeach;
			?>
			</div>
		</div>
		<div class="inner_form">
			<label for=""><?php _e('Date de congé', 'aosc'); ?></label>
			<div class="inner_form-custom">
				<input type="hidden" name="leave_date" id="leave_date" value="<?php echo get_post_meta($_GET['id_property'], 'leave_date', true); ?>">
				<div id="leave_date_display"></div>
				<div id="add_leave_date">
					<div class="inner_form-container">
						Du <input type="date" id="date_start" onchange="verif_date()" min="<?php echo date("Y-m-d");?>" value="<?php echo date("Y-m-d");?>"> 
					</div>
					<div class="inner_form-container">
						au <input type="date" id="date_end" onchange="verif_date()" min="<?php echo date("Y-m-d");?>" value="<?php echo date("Y-m-d");?>">
					</div>
					<button id="add_leave_date_btn">Ajouter</button>
				</div>
			</div>
		</div>
		<div class="inner_form">
			<label for="property_description"><?php _e('Description du bien', 'aosc'); ?></label>
			<textarea id="access_info" id="property_description" name="property_description" placeholder="Description du bien"><?php echo (isset($_GET['id_property']))? get_the_content(null, false,$_GET['id_property']):'';?></textarea>
		</div>
		<div class="inner_form">
			<label for="access_info"><?php _e('Informations d\'accès', 'aosc'); ?></label>
			<textarea id="access_info" name="access_info" id="access_info" placeholder="Information qui facilitera l'arrivée de votre locataire : se présenter à l'accueil, le code d'entrée vous sera donner la veille de votre arrivée..."><?php echo (isset($_GET['id_property']))? get_post_meta($_GET['id_property'], 'access', true):'';?></textarea>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery("principale");?>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery(1);?>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery(2);?>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery(3);?>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery(4);?>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery(5);?>
		</div>
		<!--<div class="inner_form">
			<?php input_file_img_gallery(6);?>
		</div>
		<div class="inner_form">
			<?php input_file_img_gallery(7);?>
		</div>-->

		
		<!--<div class="inner_form">
			<label for="company_desc"><?php _e('Informations de l\'entreprise', 'aosc'); ?></label>
			<textarea id="company_desc" name="company_desc" placeholder="Informations suplementaires sur l'entreprise"></textarea>
		</div>-->

		<div class="btn_container" id="time-machine">
			<input type="submit" class="button" id="owner-add-property-form-btn" value="<?php _e('Valider les informations', 'aosc'); ?>">
		</div>
		<div id="owner-add-property-form-ok" style="display:none">
			Merci d'avoir proposé votre bien sur AvecOuSansCravate! <br>Nous prenons connaissance de votre demande et revenons vers vous rapidement! 
		</div>
	
	</form>

</div>
<?php
endwhile; // End of the loop.

get_footer();


function input_file_img_gallery($nb){
	?>
	<label for="img-gallery-<?php echo $nb;?>">
		<?php 
			_e('Photo ', 'aosc');
			echo $nb;
			$size = '750-400';
			if(isset($_GET['id_property'])){

				if($nb=="principale" && has_post_thumbnail( $_GET['id_property']  )){
					echo get_the_post_thumbnail($_GET['id_property'] , 'thumbnail' );
					$image = get_post_thumbnail_id($_GET['id_property']);
					$attr_img = wp_get_attachment_image_src($image, $size);
					//var_dump($attr_img);
					if($attr_img[2] < 450){
						echo '<span style="color:red">Image trop petite</span>';
					}
					echo '<br>Supprimer cette image <input type="checkbox" name="delete_img_'.$nb.'">';
				}else{
					$image = get_field('img-gallery-'.$nb, $_GET['id_property']);
					if( $image ) {
						echo wp_get_attachment_image( $image, 'thumbnail' );
						$attr_img = wp_get_attachment_image_src($image, $size);
						if($attr_img[2] < 450){
							var_dump($image);
							echo '<span style="color:red">Image trop petite</span>';
						}
						echo '<br>Supprimer cette image <input type="checkbox" name="delete_img_'.$nb.'" value="delete">';
					}
				}
			}
		?>
	</label>
	<input type="file" id="img-gallery-<?php echo $nb;?>" name="img-gallery-<?php echo $nb;?>" id="img-gallery-<?php echo $nb;?>" accept=".jpg, .jpeg, .png">
	<?php
}