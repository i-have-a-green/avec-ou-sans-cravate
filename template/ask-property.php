<?php
/*
Template Name: ask-property
*/
$id_property = (isset($_GET['id_property'])) ? sanitize_text_field( $_GET['id_property'] ) : 0;
$user = wp_get_current_user();
get_header();
if($user->ID > 0):?>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		scroll(0, document.getElementById('section-when').offsetTop);
	});
</script>
<?php endif;
/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<!-- <div class="svg-container"> -->
<svg id="ask-property-ariane" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 102.99 276">
    <defs>
        <style>.cls-1{fill:#ee7105;}.cls-2,.cls-4{fill:none;stroke-miterlimit:10;}.cls-2{stroke:#000;}.cls-3{fill:#dbdbdb;}.cls-4{stroke:#dbdbdb;}</style>
    </defs>
    <g id="Calque_2" data-name="Calque 2">
        <g id="Calque_1-2" data-name="Calque 1">
            <g id="qui">
                <circle class="cls-1" cx="16.5" cy="16.5" r="11"/>
                <circle class="cls-2" cx="16.5" cy="16.5" r="16"/>
                <line class="cls-2" x1="16.5" y1="32.5" x2="16.5" y2="81.5"/>
                <path d="M49,19.39a5.42,5.42,0,0,0,1.39.73l-.64,1.22A3.4,3.4,0,0,1,49,21c-.06,0-.44-.28-1.15-.76a4.59,4.59,0,0,1-1.87.37,4.12,4.12,0,0,1-3.11-1.17,4.55,4.55,0,0,1-1.12-3.27,4.51,4.51,0,0,1,1.13-3.27,4.53,4.53,0,0,1,6.08,0,4.55,4.55,0,0,1,1.12,3.27,5.78,5.78,0,0,1-.31,1.95A4.2,4.2,0,0,1,49,19.39Zm-1.4-1a2.4,2.4,0,0,0,.5-.95,4.44,4.44,0,0,0,.17-1.27A3.26,3.26,0,0,0,47.63,14a2.37,2.37,0,0,0-3.43,0,3.24,3.24,0,0,0-.66,2.22,3.32,3.32,0,0,0,.66,2.25,2.11,2.11,0,0,0,1.66.75,1.87,1.87,0,0,0,.71-.13,4.63,4.63,0,0,0-1.07-.53l.49-1A5.73,5.73,0,0,1,47.62,18.41Z"/>
                <path d="M55.52,20.48v-.93a2.46,2.46,0,0,1-2.06,1.07,2.36,2.36,0,0,1-1.13-.27,1.65,1.65,0,0,1-.72-.78,3.4,3.4,0,0,1-.22-1.37V14.26H53v2.86a8.08,8.08,0,0,0,.09,1.61.92.92,0,0,0,.33.47,1,1,0,0,0,.61.17,1.3,1.3,0,0,0,.75-.23,1.16,1.16,0,0,0,.46-.58,6.8,6.8,0,0,0,.12-1.68V14.26h1.65v6.22Z"/>
                <path d="M58.76,13.42V11.89H60.4v1.53Zm0,7.06V14.26H60.4v6.22Z"/>
                <path d="M69,18.27H67.47c0-.22,0-.35,0-.39a2.61,2.61,0,0,1,.24-1.2,3.66,3.66,0,0,1,1-1,7.8,7.8,0,0,0,.86-.77,1,1,0,0,0,.22-.64,1.08,1.08,0,0,0-.38-.82,1.5,1.5,0,0,0-1-.34,1.59,1.59,0,0,0-1,.36,1.9,1.9,0,0,0-.58,1.09l-1.51-.19a2.51,2.51,0,0,1,.9-1.78,3.14,3.14,0,0,1,2.17-.74,3.26,3.26,0,0,1,2.26.75,2.23,2.23,0,0,1,.84,1.72,1.94,1.94,0,0,1-.31,1,6.59,6.59,0,0,1-1.32,1.33,2.61,2.61,0,0,0-.64.69A2.2,2.2,0,0,0,69,18.27Zm-1.49,2.21V18.84h1.64v1.64Z"/>
            </g>
            <g id="quand">
                <circle class="cls-3" cx="16.5" cy="97.5" r="11"/>
                <circle class="cls-4" cx="16.5" cy="97.5" r="16"/>
                <line class="cls-4" x1="16.5" y1="113.5" x2="16.5" y2="162.5"/>
                <path class="cls-3" d="M49,100.39a5.42,5.42,0,0,0,1.39.73l-.64,1.22A3.4,3.4,0,0,1,49,102c-.06,0-.44-.28-1.15-.76a4.59,4.59,0,0,1-1.87.37,4.12,4.12,0,0,1-3.11-1.17,5.31,5.31,0,0,1,0-6.54,4.53,4.53,0,0,1,6.08,0,4.55,4.55,0,0,1,1.12,3.27,5.78,5.78,0,0,1-.31,2A4.2,4.2,0,0,1,49,100.39Zm-1.4-1a2.4,2.4,0,0,0,.5-1,4.44,4.44,0,0,0,.17-1.27A3.26,3.26,0,0,0,47.63,95a2.37,2.37,0,0,0-3.43,0,3.24,3.24,0,0,0-.66,2.22,3.32,3.32,0,0,0,.66,2.25,2.11,2.11,0,0,0,1.66.75,1.87,1.87,0,0,0,.71-.13,4.63,4.63,0,0,0-1.07-.53l.49-1A5.73,5.73,0,0,1,47.62,99.41Z"/>
                <path class="cls-3" d="M55.52,101.48v-.93a2.46,2.46,0,0,1-2.06,1.07,2.36,2.36,0,0,1-1.13-.27,1.65,1.65,0,0,1-.72-.78,3.4,3.4,0,0,1-.22-1.37V95.26H53v2.86a8.08,8.08,0,0,0,.09,1.61.92.92,0,0,0,.33.47,1,1,0,0,0,.61.17,1.3,1.3,0,0,0,.75-.23,1.16,1.16,0,0,0,.46-.58,6.8,6.8,0,0,0,.12-1.68V95.26h1.65v6.22Z"/>
                <path class="cls-3" d="M60,97.16l-1.5-.27a2.36,2.36,0,0,1,.87-1.34,3.17,3.17,0,0,1,1.83-.43,4,4,0,0,1,1.64.26,1.62,1.62,0,0,1,.76.66,3.52,3.52,0,0,1,.22,1.48l0,1.92a6.09,6.09,0,0,0,.08,1.21,3.44,3.44,0,0,0,.29.83H62.53q-.06-.17-.15-.48l-.06-.2a2.91,2.91,0,0,1-.9.62,2.74,2.74,0,0,1-1,.2,2.08,2.08,0,0,1-1.51-.52,1.74,1.74,0,0,1-.56-1.32,1.86,1.86,0,0,1,.25-.94,1.74,1.74,0,0,1,.71-.63,5.38,5.38,0,0,1,1.31-.38,9,9,0,0,0,1.6-.41v-.16a.85.85,0,0,0-.24-.68,1.42,1.42,0,0,0-.88-.2,1.26,1.26,0,0,0-.69.17A1.24,1.24,0,0,0,60,97.16Zm2.2,1.33c-.21.07-.55.16-1,.26a3,3,0,0,0-.9.28.71.71,0,0,0-.32.58.79.79,0,0,0,.26.6.88.88,0,0,0,.65.25,1.39,1.39,0,0,0,.85-.29,1,1,0,0,0,.39-.54,3,3,0,0,0,.07-.81Z"/>
                <path class="cls-3" d="M71.09,101.48H69.44V98.31a5,5,0,0,0-.1-1.31,1,1,0,0,0-.35-.46.94.94,0,0,0-.57-.16,1.34,1.34,0,0,0-.76.23,1.19,1.19,0,0,0-.47.62,5.54,5.54,0,0,0-.13,1.43v2.82H65.42V95.26H67v.91a2.48,2.48,0,0,1,2-1,2.31,2.31,0,0,1,1,.2,1.67,1.67,0,0,1,.69.5,1.82,1.82,0,0,1,.32.69,5.17,5.17,0,0,1,.09,1.11Z"/>
                <path class="cls-3" d="M78.47,101.48H76.94v-.91a2.57,2.57,0,0,1-.9.79,2.26,2.26,0,0,1-1.05.26,2.4,2.4,0,0,1-1.84-.86,3.57,3.57,0,0,1-.76-2.41,3.51,3.51,0,0,1,.74-2.41A2.44,2.44,0,0,1,75,95.12a2.33,2.33,0,0,1,1.8.87v-3.1h1.65Zm-4.4-3.24a2.79,2.79,0,0,0,.28,1.44,1.25,1.25,0,0,0,2.08.16,2.26,2.26,0,0,0,.4-1.45,2.43,2.43,0,0,0-.39-1.54,1.24,1.24,0,0,0-1-.47,1.23,1.23,0,0,0-1,.46A2.11,2.11,0,0,0,74.07,98.24Z"/>
                <path class="cls-3" d="M87,99.27H85.47c0-.22,0-.35,0-.39a2.61,2.61,0,0,1,.24-1.2,3.66,3.66,0,0,1,1-1.05,7.8,7.8,0,0,0,.86-.77,1,1,0,0,0,.22-.64,1.08,1.08,0,0,0-.38-.82,1.5,1.5,0,0,0-1-.34,1.59,1.59,0,0,0-1,.36,1.9,1.9,0,0,0-.58,1.09l-1.51-.19a2.51,2.51,0,0,1,.9-1.78,3.14,3.14,0,0,1,2.17-.74,3.26,3.26,0,0,1,2.26.75,2.23,2.23,0,0,1,.84,1.72,1.94,1.94,0,0,1-.31,1,6.59,6.59,0,0,1-1.32,1.33,2.61,2.61,0,0,0-.64.69A2.2,2.2,0,0,0,87,99.27Zm-1.49,2.21V99.84h1.64v1.64Z"/>
            </g>
            <g id="quoi">
                <circle class="cls-3" cx="16.5" cy="178.5" r="11"/>
                <circle class="cls-4" cx="16.5" cy="178.5" r="16"/>
                <line class="cls-4" x1="16.5" y1="194.5" x2="16.5" y2="243.5"/>
                <path class="cls-3" d="M49.35,181.06a5.91,5.91,0,0,0,1.39.72L50.1,183a4.64,4.64,0,0,1-.77-.32l-1.15-.77a4.56,4.56,0,0,1-1.87.37,4.09,4.09,0,0,1-3.1-1.16,5.35,5.35,0,0,1,0-6.55,4,4,0,0,1,3-1.17,4,4,0,0,1,3,1.17,4.52,4.52,0,0,1,1.12,3.27,5.6,5.6,0,0,1-.31,2A3.82,3.82,0,0,1,49.35,181.06Zm-1.39-1a2.61,2.61,0,0,0,.5-1,4.51,4.51,0,0,0,.17-1.28,3.24,3.24,0,0,0-.66-2.22,2.37,2.37,0,0,0-3.44,0,3.24,3.24,0,0,0-.66,2.22,3.33,3.33,0,0,0,.66,2.26,2.16,2.16,0,0,0,1.67.74,2.08,2.08,0,0,0,.71-.12,5.12,5.12,0,0,0-1.07-.54l.48-1A6.08,6.08,0,0,1,48,180.08Z"/>
                <path class="cls-3" d="M55.86,182.15v-.93a2.43,2.43,0,0,1-.9.78,2.53,2.53,0,0,1-1.17.29,2.22,2.22,0,0,1-1.12-.28,1.63,1.63,0,0,1-.72-.77,3.43,3.43,0,0,1-.22-1.38v-3.94h1.64v2.86a7.93,7.93,0,0,0,.09,1.61.85.85,0,0,0,.33.47,1,1,0,0,0,.61.17,1.26,1.26,0,0,0,.76-.23,1.21,1.21,0,0,0,.46-.57,6.85,6.85,0,0,0,.12-1.68v-2.63h1.65v6.23Z"/>
                <path class="cls-3" d="M58.71,179a3.35,3.35,0,0,1,.4-1.59,2.82,2.82,0,0,1,1.15-1.17,3.39,3.39,0,0,1,1.65-.41,3.11,3.11,0,0,1,2.32.92,3.45,3.45,0,0,1,0,4.66,3.08,3.08,0,0,1-2.29.93,3.63,3.63,0,0,1-1.64-.39,2.69,2.69,0,0,1-1.18-1.13A3.79,3.79,0,0,1,58.71,179Zm1.69.09a2,2,0,0,0,.44,1.41,1.42,1.42,0,0,0,2.16,0,2.5,2.5,0,0,0,0-2.83,1.44,1.44,0,0,0-2.16,0A2,2,0,0,0,60.4,179Z"/>
                <path class="cls-3" d="M66.42,175.08v-1.52h1.65v1.52Zm0,7.07v-6.23h1.65v6.23Z"/>
                <path class="cls-3" d="M76.63,179.93h-1.5v-.39a2.67,2.67,0,0,1,.24-1.2,3.66,3.66,0,0,1,1-1.05,6.84,6.84,0,0,0,.86-.77,1,1,0,0,0-.17-1.45,1.48,1.48,0,0,0-1-.35,1.56,1.56,0,0,0-1,.36,1.94,1.94,0,0,0-.58,1.09L72.85,176a2.5,2.5,0,0,1,.89-1.78,3.16,3.16,0,0,1,2.18-.73,3.28,3.28,0,0,1,2.25.74,2.25,2.25,0,0,1,.84,1.73,1.88,1.88,0,0,1-.31,1,6.75,6.75,0,0,1-1.31,1.32,2.55,2.55,0,0,0-.65.7A2.39,2.39,0,0,0,76.63,179.93Zm-1.5,2.22V180.5h1.65v1.65Z"/>
            </g>
            <g id="combien">
                <circle class="cls-3" cx="16.5" cy="259.5" r="11"/>
                <circle class="cls-4" cx="16.5" cy="259.5" r="16"/>
                <path class="cls-3" d="M47.91,260.37l1.68.54A3.83,3.83,0,0,1,48.3,263a4,4,0,0,1-5.09-.49,4.46,4.46,0,0,1-1.1-3.2,4.73,4.73,0,0,1,1.1-3.33,3.81,3.81,0,0,1,2.92-1.19,3.59,3.59,0,0,1,2.56.94,3.38,3.38,0,0,1,.88,1.58l-1.72.41a1.79,1.79,0,0,0-.64-1.06,1.85,1.85,0,0,0-1.17-.38,2,2,0,0,0-1.55.68,3.35,3.35,0,0,0-.6,2.22,3.59,3.59,0,0,0,.59,2.32,1.91,1.91,0,0,0,1.52.7,1.75,1.75,0,0,0,1.19-.44A2.65,2.65,0,0,0,47.91,260.37Z"/>
                <path class="cls-3" d="M50.69,260.33a3.41,3.41,0,0,1,.4-1.59,2.8,2.8,0,0,1,1.14-1.17,3.42,3.42,0,0,1,1.66-.4,3.1,3.1,0,0,1,2.31.91,3.17,3.17,0,0,1,.91,2.32,3.24,3.24,0,0,1-.91,2.34,3.1,3.1,0,0,1-2.3.93,3.71,3.71,0,0,1-1.63-.38,2.66,2.66,0,0,1-1.18-1.14A3.77,3.77,0,0,1,50.69,260.33Zm1.68.09a2.08,2.08,0,0,0,.44,1.42,1.4,1.4,0,0,0,1.09.49,1.38,1.38,0,0,0,1.08-.49,2.11,2.11,0,0,0,.43-1.43A2.07,2.07,0,0,0,55,259a1.38,1.38,0,0,0-1.08-.49,1.4,1.4,0,0,0-1.09.49A2.08,2.08,0,0,0,52.37,260.42Z"/>
                <path class="cls-3" d="M58.27,257.31h1.52v.85a2.44,2.44,0,0,1,1.94-1,2.16,2.16,0,0,1,1,.24,2,2,0,0,1,.72.75,3,3,0,0,1,.88-.75,2.42,2.42,0,0,1,2.18,0,1.71,1.71,0,0,1,.71.81,3.66,3.66,0,0,1,.17,1.29v4H65.78V260a2.69,2.69,0,0,0-.17-1.19.77.77,0,0,0-.7-.35,1.12,1.12,0,0,0-.65.21,1.14,1.14,0,0,0-.44.62,4.38,4.38,0,0,0-.14,1.28v3H62v-3.41A4.83,4.83,0,0,0,62,259a.69.69,0,0,0-.28-.39.82.82,0,0,0-.5-.13,1.23,1.23,0,0,0-.68.2,1.14,1.14,0,0,0-.44.59,4.39,4.39,0,0,0-.13,1.29v3H58.27Z"/>
                <path class="cls-3" d="M69,263.53v-8.59h1.64V258a2.33,2.33,0,0,1,1.81-.87,2.43,2.43,0,0,1,1.88.82,3.45,3.45,0,0,1,.74,2.37,3.56,3.56,0,0,1-.76,2.45,2.37,2.37,0,0,1-1.84.86,2.33,2.33,0,0,1-1-.26,2.6,2.6,0,0,1-.9-.79v.91Zm1.63-3.24a2.53,2.53,0,0,0,.31,1.42,1.29,1.29,0,0,0,1.13.66,1.16,1.16,0,0,0,.93-.46,2.29,2.29,0,0,0,.38-1.47A2.42,2.42,0,0,0,73,258.9a1.21,1.21,0,0,0-1-.47,1.23,1.23,0,0,0-1,.46A2.11,2.11,0,0,0,70.63,260.29Z"/>
                <path class="cls-3" d="M76.4,256.46v-1.52H78v1.52Zm0,7.07v-6.22H78v6.22Z"/>
                <path class="cls-3" d="M83.33,261.55l1.64.28a2.68,2.68,0,0,1-1,1.37,3,3,0,0,1-1.71.47,2.79,2.79,0,0,1-2.41-1.06,3.56,3.56,0,0,1-.61-2.14,3.46,3.46,0,0,1,.81-2.43,2.67,2.67,0,0,1,2-.87,2.75,2.75,0,0,1,2.19.91,4,4,0,0,1,.77,2.81H80.94a1.62,1.62,0,0,0,.4,1.14,1.21,1.21,0,0,0,.94.41,1,1,0,0,0,.66-.21A1.35,1.35,0,0,0,83.33,261.55Zm.1-1.66a1.6,1.6,0,0,0-.37-1.09,1.14,1.14,0,0,0-.86-.37,1.15,1.15,0,0,0-.89.39,1.51,1.51,0,0,0-.34,1.07Z"/>
                <path class="cls-3" d="M92.06,263.53H90.42v-3.17a4.88,4.88,0,0,0-.11-1.31.89.89,0,0,0-.34-.46,1,1,0,0,0-.57-.16,1.37,1.37,0,0,0-.77.23,1.25,1.25,0,0,0-.47.62,5.67,5.67,0,0,0-.12,1.43v2.82H86.39v-6.22h1.53v.91a2.58,2.58,0,0,1,3-.86,1.65,1.65,0,0,1,1,1.2,5.06,5.06,0,0,1,.09,1.1Z"/>
                <path class="cls-3" d="M100.61,261.32h-1.5v-.4a2.6,2.6,0,0,1,.24-1.19,3.67,3.67,0,0,1,1-1.06,7.12,7.12,0,0,0,.86-.76,1.1,1.1,0,0,0,.22-.64,1.06,1.06,0,0,0-.39-.82,1.47,1.47,0,0,0-1-.34,1.6,1.6,0,0,0-1,.35,1.91,1.91,0,0,0-.58,1.09l-1.51-.18a2.48,2.48,0,0,1,.89-1.78,3.16,3.16,0,0,1,2.18-.74,3.28,3.28,0,0,1,2.25.74,2.25,2.25,0,0,1,.84,1.73,1.85,1.85,0,0,1-.31,1,6.47,6.47,0,0,1-1.31,1.33,2.54,2.54,0,0,0-.65.69A2.42,2.42,0,0,0,100.61,261.32Zm-1.5,2.21v-1.65h1.65v1.65Z"/>
            </g>
        </g>
    </g>
</svg>
<!-- </div> -->
	
	<form action="" method="post"  name="ask_property_form" id="ask_property_form" class="form-style alignwide">
	<input type="hidden" name="honeyPot" value="">
		<input type="hidden" name="id_property" value="<?php echo $id_property;?>">
		<input type="hidden" name="redirect" id="redirect" value="<?php echo get_post_type_archive_link( 'property' ); ?>">
		
		<section id="section-user" >
			<div class="centering_wrapper">
				<?php if(isset($_GET['id_property'])): ?>
					<h2><?php _e("Vous souhaitez des informations sur ", 'aosc'); echo get_the_title($id_property);?></h2>
				<?php else: ?>
					<h2><?php _e("Vous cherchez un lieu pour votre événement", 'aosc');?></h2>
					<p><b><?php _e("Gagnez du temps en nous confiant votre recherche gratuitement !","aosc");?></b></p>
				<?php endif;?>
				<?php the_content(); ?>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color" style="margin-bottom:1rem;">
				<h2><?php _e("Vos coordonnées", 'aosc');?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="inner_section">
					<div class="inner_form">
						<label for="user_lastname"><?php _e('Nom *', 'aosc'); ?></label>
						<input type="text" id="user_lastname" name="user_lastname" placeholder="Saisissez votre nom" value="<?php echo $user->user_lastname;?>" required>
					</div>
		
					<div class="inner_form">
						<label for="user_firstname"><?php _e('Prénom *', 'aosc'); ?></label>
						<input type="text" id="user_firstname" name="user_firstname" placeholder="Saisissez votre prénom" value="<?php echo $user->user_firstname;?>" required>
					</div>
		
					<div class="inner_form">
						<label for="user_email"><?php _e('Adresse Email *', 'aosc'); ?></label>
						<input type="email" id="user_email" name="user_email" placeholder="Saisissez votre adresse email" value="<?php echo $user->user_email;?>" required>
					</div>
					
					<div class="inner_form">
						<label for="user_phone"><?php _e('Téléphone *', 'aosc'); ?></label>
						<input type="text" pattern="[0-9]{10}"  title="10 chiffres sans espace, sans tiret" id="user_phone" name="user_phone" placeholder="0601020304" value="<?php echo get_user_meta($user->ID, 'user_phone', true);?>"  required>
					</div>
					
					<div class="btn_container">
						<a class="button" href="#section-when"><?php _e('Étape suivante', 'aosc'); ?></a>
					</div>

				</div>
			</div>
			
		</section>

		<section id="section-when">
			<div class="centering_wrapper">
				<div class="inner_section">
				<h2><?php _e("Quand souhaitez vous travailler ? ", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="inner_section">
					<div class="inner_form">
						<label for="ask_date"><?php _e("Date souhaitée", 'aosc');?></label>
						<input type="date" name="ask_date" id="ask_date">
					</div>
					<div class="inner_form checkboxes">
						<label for="ask_time_slot"><?php _e("Créneau horaire", 'aosc');?></label>
						<div class="checkboxes_custom">
							<div class="solo_checkbox"><input type="checkbox" name="ask_time_slot[]" value="morning" ><?php _e("Matin (8h - 12h)");?></div>
							<div class="solo_checkbox"><input type="checkbox" name="ask_time_slot[]" value="afternoon"><?php _e("Après-midi (14h - 19h)");?></div>
							<div class="solo_checkbox"><input type="checkbox" name="ask_time_slot[]" value="evening"><?php _e("Soirée (19h - 22h)");?></div>
						</div>
					</div>
					<div class="inner_form">
						<div class="select">
							<label for="date_during"><?php _e('Durée d\'occupation souhaitée', 'aosc'); ?></label>
							<select name="date_during" id="date_during">
								<option></option>
								<option value="hour">Une heure</option>
								<option value="hours">Deux heures</option>
								<option value="half-day">Une demi-journée</option>
								<option value="day">Une journée</option>
								<option value="week">Une semaine</option>
							</select>
						</div>
					</div>
					<div class="btn_container">
						<a class="button" href="#section-user"><?php _e('Étape précédente', 'aosc'); ?></a>
						<a class="button" href="#section-what"><?php _e('Étape suivante', 'aosc'); ?></a>
					</div>
				</div>
			</div>
		</section>

		<section id="section-what">
			<div class="centering_wrapper">
				<h2><?php _e("Votre besoin ? ", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="custom_need_form">
					<input type="hidden" name="ask_what" id="ask_what" value="" required>
					<span onclick="change_ask_what(this, 'Travailler mes dossiers hors de chez moi');" class="ask_what"><?php _e("Travailler mes dossiers hors de chez moi", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'Réalisez un afterwoork - soirée d＇entreprise - pot de départ');" class="ask_what"><?php _e("Réalisez un afterwoork/soirée d'entreprise/pot de départ", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'Organiser un séminaire - une réunion');" class="ask_what"><?php _e("Organiser un séminaire / une réunion", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'Recevoir un(des) client(s) / partenaire(s) en rendez-vous');" class="ask_what"><?php _e("Recevoir un(des) client(s) / partenaire(s) en rendez-vous", 'aosc');?></span>
					<!-- <span onclick="change_ask_what(this, 'réunion');" class="ask_what"><?php _e("Organiser une réunion de travail", 'aosc');?></span> -->
					<span onclick="change_ask_what(this, 'Activité de bien être');" class="ask_what"><?php _e("Activité de bien être", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'Autre');" class="ask_what"><?php _e("Autre", 'aosc');?></span>
				</div>
				<div class="btn_container">
					<a class="button" href="#section-when"><?php _e('Étape précédente', 'aosc'); ?></a>
					<a class="button" href="#section-how-many"><?php _e('Étape suivante', 'aosc'); ?></a>
				</div>
			</div>
		</section>

		<section id="section-how-many">
			<div class="centering_wrapper">
				<h2><?php _e("Participants", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="inner_section">
					<div class="inner_form">
						<label for="ask_participants_number"><?php _e("Nombre de personnes", 'aosc');?></label>
						<input type="number" min="1" name="ask_participants_number" id="ask_participants_number">
					</div>
				</div>
	
				<h2><?php _e("Des précisions à apporter ? ", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="inner_section">
					<div class="inner_form">
						<label for="ask_comment"><?php _e("Entrez ici vos remarqes", 'aosc');?></label>
						<textarea name="ask_comment" id="ask_comment" placeholder="Tapez votre message"></textarea>
					</div>
				</div>

									<!--<div class="checkbox form-item">
						<input type="checkbox" name="ok_cgu" id="ok_cgu" required>
						<div class="checkbox-label">
							<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles.', 'aosc');?>*</label>
							<div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'aosc') ;?></a></div>
						</div>
					</div>
		
					<p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'aosc'); ?></p>-->

					<div class="btn_container">
						<a class="button" href="#section-what"><?php _e('Étape précédente', 'aosc'); ?></a>
						<input type="submit" class="button" id="ask_property_submit_button" value="<?php _e('Envoyez votre demande', 'aosc'); ?>">
					</div>
					<h3 style="display:none;" id="ask_property_send_ok">
						<?php _e('Votre demande a bien été envoyée.','aosc');?>
					</h3>
				
			</div>
		</section>

		
	</form>
	<?php
endwhile; // End of the loop.

get_footer();
