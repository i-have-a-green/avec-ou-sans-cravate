<?php
/*
Template Name: coworker-liked-property
*/
$user = secure_role('coworker');
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="coworker-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-coworker' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<?php the_content();?>
	
	<?php 
		global $post;
		$wishlist = get_user_meta($user->id, 'wishlist', true);

		if (!is_array($wishlist) || sizeof($wishlist) == 0) {
			?>
				<h3 style="text-align:center;">Ajoutez dès maintenant vos premiers coups de coeur 🧡 </h3>
			<?php
		}else{
			?>			
			<div class="property_grid">
			<?php
				foreach($wishlist as $property_id){
					$post = get_post($property_id);
					setup_postdata( $post );
					set_query_var( 'whishlistUser', $wishlist );
					get_template_part( 'template-parts/content/content', 'property' ); 
				}
				wp_reset_postdata();
			?>
			</div><?php
		}
		?>
</div>


<?php
endwhile; // End of the loop.

get_footer();
