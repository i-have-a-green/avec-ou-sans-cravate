<?php
/*
Template Name: coworker-history
*/
$user = secure_role('coworker');
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="coworker-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-coworker' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<div class="last-history-list">
		<?php
		$args = array(
			'author__in'=> array($user->ID),
			'meta_key' => 'ask_date',
			'orderby'	=> 'meta_value',
			'order'		=> 'ASC'
		);
		$comments = get_comments( $args );
		foreach ( $comments as $comment ) :
			set_query_var( 'comment', $comment );
			get_template_part( 'template-parts/content/content-comment' );
		endforeach;
		?>
	</div>

</div>

<?php
endwhile; // End of the loop.

get_footer();
