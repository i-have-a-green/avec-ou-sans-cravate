<?php
/*
Template Name: coworker-profile
*/
$user = secure_role('coworker');
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="coworker-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-coworker' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<form action="" method="post"  name="coworker-profile-form" id="coworker-profile-form">
		<input type="hidden" name="honeyPot" value="">

		
		<div class="inner_form"> 
			<label for="user_firstname"><?php _e('Prénom *', 'aosc'); ?></label>
			<input type="text" id="user_firstname" name="user_firstname" placeholder="Saisissez votre prénom"  value="<?php echo $user->first_name;?>" required>
		</div>
		
		<div class="inner_form">
			<label for="user_lastname"><?php _e('Nom *', 'aosc'); ?></label>
			<input type="text" id="user_lastname" name="user_lastname" placeholder="Saisissez votre nom" value="<?php echo $user->last_name;?>" required>
		</div>

		

		<div class="inner_form">
			<label for="user_email"><?php _e('Adresse Email *', 'aosc'); ?></label>
			<input type="email" id="user_email" name="user_email"  value="<?php echo $user->user_email;?>" required>
		</div>
		
		<div class="inner_form">
			<label for="user_phone"><?php _e('Téléphone *', 'aosc'); ?></label>
			<input type="text" pattern="[0-9]{10}"  title="10 chiffres sans espace, sans tiret" id="user_phone" name="user_phone"  value="<?php echo get_user_meta($user->ID, 'user_phone', true );?>"  required>
		</div>
		<div class="inner_form">
			<label for="user_lk"><?php _e('Profil linkedin', 'aosc'); ?></label>
			<input type="text" id="linkedin" name="linkedin" placeholder="https://www.linkedin.com/in/" value="<?php echo get_user_meta($user->ID, 'linkedin', true );?>">
		</div>
		<div class="inner_form">
			<label for="user_status"><?php _e('Fonction', 'aosc'); ?></label>
			<input type="text" id="user_status" name="user_status" placeholder="Indiquez votre fonction au sein de l'entreprise"  value="<?php echo get_user_meta($user->ID, 'user_status', true );?>">
		</div>
		<div class="inner_form">
			<label for="company_name"><?php _e('Entreprise', 'aosc'); ?></label>
			<input type="text" id="company_name" name="user_company" placeholder="" value="<?php echo get_user_meta($user->ID, 'user_company', true );?>">
		</div>

		<div class="inner_form">
			<label for="user_adress"><?php _e('Adresse de l\'entreprise', 'aosc'); ?></label>
			<input type="text" id="user_adress" name="user_address" placeholder="Saisissez l'adresse postale de l'entreprise"  value="<?php echo get_user_meta($user->ID, 'user_address', true );?>">
		</div>



		<!--<div class="inner_form">
			<label for="company_description"><?php _e('Description de l\'entreprise (s\'affiche sur le site)', 'aosc'); ?></label>
			<textarea id="company_description" id="company_description" name="company_description" placeholder="Description de l'entreprise"><?php echo $user->description;?></textarea>
		</div>-->

		<div class="inner_form">
			<label for="company_activity"><?php _e('Activité', 'aosc'); ?></label>
			<input type="text" id="company_activity" name="company_activity" placeholder="" value="<?php echo get_user_meta($user->ID, 'company_activity', true );?>">
		</div>

		<!--<div class="inner_form">
			<label for="company_target"><?php _e('Coeur de cible', 'aosc'); ?></label>
			<input type="text" id="company_target" name="company_target" placeholder="" value="<?php echo get_user_meta($user->ID, 'company_target', true );?>">
		</div>-->

		<div class="btn_container">
			<input type="submit" class="button" id="coworker-profile-form-btn" value="<?php _e('Valider les informations', 'aosc'); ?>">
		</div>
		<div id="coworker-profile-form-ok" style="display:none">
			Votre demande a été prise en compte
		</div>
	</form>
	
</div>

<?php
endwhile; // End of the loop.

get_footer();
