<?php
/*
Template Name: pre-owner-sign-in
*/

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();

	?>
<section class="aligndefault">
		<?php the_title( '<h2 class="entry-title aligncenter custom-h2">', '</h2>' ); ?>
		
		<div class="content">
			<?php the_content(); ?>
		</div>
	
		<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color" style="margin:2rem auto;">
	
		<form action="" method="post"  name="pre-owner-form" id="pre-owner-form" class="form-style">
		<input type="hidden" name="honeyPot" value="">
	
			<div class="inner_form">
				<label for="user_lastname"><?php _e('Nom *', 'aosc'); ?></label>
				<input type="text" id="user_lastname" name="user_lastname" placeholder="Saisissez votre nom" value="" required>
			</div>
	
			<div class="inner_form">
				<label for="user_firstname"><?php _e('Prénom *', 'aosc'); ?></label>
				<input type="text" id="user_firstname" name="user_firstname" placeholder="Saisissez votre prénom" value="" required>
			</div>
	
			<div class="inner_form">
				<label for="user_company"><?php _e('Nom de l\'entité', 'aosc'); ?></label>
				<input type="text" id="user_company" name="user_company" placeholder="Saisissez le nom de votre entreprise ou collectivité" value="" >
			</div>
	
			<div class="inner_form">
				<label for="user_email"><?php _e('Adresse Email *', 'aosc'); ?></label>
				<input type="email" id="user_email" name="user_email" placeholder="Saisissez votre adresse email" value="" required>
			</div>
			
			<div class="inner_form">
				<label for="user_phone"><?php _e('Téléphone *', 'aosc'); ?></label>
				<input type="text" pattern="[0-9]{10}"  title="10 chiffres sans espace, sans tiret"  id="user_phone" name="user_phone" placeholder="0102030405" value=""  required>
			</div>
	
			<div class="inner_form">
				<label for="user_address"><?php _e('Adresse *', 'aosc'); ?></label>
				<input type="text" id="user_address" name="user_address" placeholder="Saisissez votre adresse" value="">
			</div>
	
			<div class="inner_form">
				<div class="select chackboxselect">
					<label for="type_property"><?php _e('Type de bien *', 'aosc'); ?></label>
						<div class="container_checkbox">
							<div class="grp">
								<input type="checkbox" name="type_property[]" value="Espace partagé - Open Space" >Espace partagé / Open Space<br>
							</div>
							<div class="grp">
								
								<input type="checkbox" name="type_property[]" value="Bureau Privatif">Bureau Privatif<br>
							</div>
							<div class="grp">
								
								<input type="checkbox" name="type_property[]" value="Salle de réunion / Lieu de séminaire">Salle de réunion / Lieu de séminaire<br>
							</div>
							<div class="grp">
								
								<input type="checkbox" name="type_property[]" value="Autres">Autres<br>
							</div>
						</div>
				</div>
			</div>
	
			<div class="btn_container">
				<input type="submit" class="button" id="pre-owner-form-btn" value="<?php _e('Valider les informations', 'aosc'); ?>">
			</div>
			<div id="pre-owner-form-ok" style="display:none;">
				<?php _e('Votre demande a bien été envoyée.','aosc');?>
			</div>
		</form>
					</section>
	
	<?php
endwhile; // End of the loop.

get_footer();
