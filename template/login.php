<?php
/*
Template Name: login
*/
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
?>
<div class="aligndefault login">
	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>	
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
<?php
	
	the_content();

	$redirect = get_permalink(get_field('dashboard-coworker', 'option'));
    $referrer = $_SERVER['HTTP_REFERER'];
    $qs = parse_url($referrer, PHP_URL_QUERY);
    if(!empty($qs)){
        parse_str($qs, $output);
        $redirect = get_permalink($output['id_property']);
    }

	if(isset($_GET['login']) && $_GET['login'] == 'failed'){
		echo '<p style="color:red;text-align:center">Identifiant ou Mot de passe incorrect</p>';
	}


	wp_login_form(
		array(
			'remember'       => false,
			'label_username' => __( 'Adresse email', 'aosc' ),
			'label_password' => __( 'Mot de passe', 'aosc' ),
			'redirect'       => $redirect
		)
	);?>
	<!-- <span id="pwdView">Voir le mot de passe.</span> -->
	<a class="lost-pswd" href="<?php echo wp_lostpassword_url(get_home_url()) ; ?>"><?php _e("Mot de passe oublié ?", "aosc"); ?></a>
</div>
	<?php
endwhile; // End of the loop.

get_footer();
