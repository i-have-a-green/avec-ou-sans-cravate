<?php
/*
Template Name: owner-dashboard
*/
$user = secure_role('owner');
$properties = get_posts( array( 
	'author' 	=> $user->ID,
	'post_type'	=> 'property',
	'numberposts' => -1,
	) 
);
$comments = array();
$nbr_property = count($properties);
$comments_count = 0;
$tab_properties_id = array();
if($nbr_property > 0){
	foreach($properties as $property){
		$tab_properties_id[] = $property->ID;
	}
	$args = array(
		'post__in' => $tab_properties_id,   
		'orderby'	=> 'comment_date',
		'order'	=> 'ASC',
	);

	$comments = get_comments( $args );
}


get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="owner-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-owner' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<div class="alignnarrow custom-roundblock">
		<p class="h4">Date d'adhésion : <span><?php echo date_i18n( get_option( 'date_format' ), strtotime( $user->user_registered ) ); ?></span></p>
		<p class="h4">Nombre de contacts reçus : <span><?php echo sizeof($comments);?></span></p>
		<p class="h4">Nombre de bien(s) en location : <span><?php echo $nbr_property;?></span></p>
		<!--<p class="h4">Nombre de bien(s) en favoris : <span> 02</span></p>-->
	</div>
	<?php if(sizeof($comments) > 0):?>
	<h2 class="entry-title aligncenter">Mes dernières demandes</h2>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<div class="last-ask-list">
		<?php 
		for($i=0;$i<3 && $i<sizeof($comments);$i++){
			set_query_var( 'comment', $comments[$i] );
			get_template_part( 'template-parts/content/content-comment' );
		}

		
		?>
	</div>
	<?php endif;?>
</div>

<?php
endwhile; // End of the loop.

get_footer();
