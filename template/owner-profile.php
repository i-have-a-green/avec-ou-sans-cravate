<?php
/*
Template Name: owner-profile
*/
$user = secure_role('owner');
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>
	
<div class="owner-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-owner' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<form action="" method="post"  name="owner-profile-form" id="owner-profile-form">
		<input type="hidden" name="honeyPot" value="">

		<div class="inner_form">
			<label for="user_firstname"><?php _e('Prénom *', 'aosc'); ?><br><small><em>Publié sur la fiche de bien</em></small></label>
			<input type="text" id="user_firstname" name="user_firstname" placeholder="Saisissez votre prénom" value="<?php echo $user->first_name;?>" required>
		</div>
		<div class="inner_form">
			<label for="user_lastname"><?php _e('Nom *', 'aosc'); ?><br><small><em>Publié sur la fiche de bien</em></small></label>
			<input type="text" id="user_lastname" name="user_lastname" placeholder="Saisissez votre nom" value="<?php echo $user->last_name;?>" required>
		</div>


		<div class="inner_form">
			<label for="user_email"><?php _e('Adresse mail *', 'aosc'); ?></label>
			<input type="email" id="user_email" name="user_email" placeholder="Saisissez votre adresse email" value="<?php echo $user->user_email;?>" required>
		</div>
		 
		<div class="inner_form">
			<label for="user_phone"><?php _e('Téléphone *', 'aosc'); ?><br><small><em>Publié sur la fiche de bien</em></small></label>
			<input type="text" pattern="[0-9]{10}"  title="10 chiffres sans espace, sans tiret"  id="user_phone" name="user_phone" placeholder="0102030405" value="<?php echo get_user_meta($user->ID, 'user_phone', true );?>"  required>
		</div>
		<div class="inner_form">
			<label for="company_name"><?php _e('Entreprise', 'aosc'); ?></label>
			<input type="text" id="company_name" name="user_company" placeholder="" value="<?php echo get_user_meta($user->ID, 'user_company', true );?>">
		</div>
		<div class="inner_form">
			<label for="user_lk"><?php _e('Profil linkedin', 'aosc'); ?></label>
			<input type="text" id="linkedin" name="linkedin" placeholder="https://www.linkedin.com/in/" value="<?php echo get_user_meta($user->ID, 'linkedin', true );?>">
		</div>

		<div class="inner_form">
			<label for="user_adress"><?php _e('Adresse', 'aosc'); ?></label>
			<input type="text" id="user_adress" name="user_address" placeholder="Saisissez l'adresse postale de l'entreprise" value="<?php echo get_user_meta($user->ID, 'user_address', true );?>">
		</div>

		<div class="inner_form">
			<label for="company_description"><?php _e('Description de l\'entreprise', 'aosc'); ?><br><small><em>Publié sur la fiche de bien</em></small></label>
			<textarea id="company_description" id="company_description" name="company_description" placeholder="Histoire, produits ou services ..."><?php echo $user->description;?></textarea>
		</div>

		<div class="inner_form">
			<label for="company_activity"><?php _e('Activité', 'aosc'); ?></label>
			<input type="text" id="company_activity" name="company_activity" placeholder="" value="<?php echo get_user_meta($user->ID, 'company_activity', true );?>">
		</div>

		<!--<div class="inner_form">
			<label for="company_target"><?php _e('Coeur de cible', 'aosc'); ?></label>
			<input type="text" id="company_target" name="company_target" placeholder="" value="<?php echo get_user_meta($user->ID, 'company_target', true );?>">
		</div>-->


		<div class="inner_form">
            <label for="user_pwd"><?php _e('Mot de passe<br>Laisser vide si pas de changement', 'aosc'); ?></label>
            <input type="password" id="user_pwd" name="user_pwd" placeholder="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                title="<?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "aosc");?>" >
			<span id="pwdView">Voir le mot de passe.</span>
        </div>
        <p class="form-info form-sub-item"><?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "cwcud");?></p>

		<div class="btn_container">
			<input type="submit" id="owner-profile-form-btn" value="<?php _e('Valider les informations', 'aosc'); ?>">
		</div>
		<div id="owner-profile-form-ok" style="display:none">
			Votre demande a été prise en compte
		</div>

	</form>
	
</div>

<?php
endwhile; // End of the loop.

get_footer();
