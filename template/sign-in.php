<?php
/*
Template Name: sign-in
*/

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
    ?>
    <div class="aligndefault login">
        <?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>	
        <hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
<?php
    $redirect = get_permalink(get_field('dashboard-coworker', 'option'));
    $referrer = $_SERVER['HTTP_REFERER'];
    $qs = parse_url($referrer, PHP_URL_QUERY);
    if(!empty($qs)){
        parse_str($qs, $output);
        $redirect = get_permalink($output['id_property']);
    }
	
	?>
	<form action="" method="post"  name="sign-in-coworker-form" id="sign-in-coworker-form" class="form-style">
        
        <input type="hidden" name="honeyPot" value="">
        <input type="hidden" name="redirect" id="redirect" value="<?php echo $redirect;?>">

        <div class="inner_form">
            <label for="user_firstname"><?php _e('Prénom *', 'aosc'); ?></label>
            <input type="text" id="user_firstname" name="user_firstname" placeholder="Saisissez votre prénom" required>
        </div>

        <div class="inner_form">
            <label for="user_lastname"><?php _e('Nom *', 'aosc'); ?></label>
            <input type="text" id="user_lastname" name="user_lastname" placeholder="Saisissez votre nom" required>
        </div>

        <div class="inner_form">
            <label for="user_email"><?php _e('Adresse Email *', 'aosc'); ?></label>
            <input type="email" id="user_email" name="user_email" placeholder="Saisissez votre adresse email" required>
        </div>
        <p class="form-info form-sub-item"><?php _e('* Votre adresse mail servira d\'identifiant pour votre compte.', 'aosc');?></p>

        <div class="inner_form">
            <label for="user_pwd"><?php _e('Mot de passe *', 'aosc'); ?></label>
            <input type="password" id="user_pwd" name="user_pwd" placeholder="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                title="<?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "aosc");?>" required>
            <span id="pwdView">Voir le mot de passe.</span>    
        </div>
        <p class="form-info form-sub-item"><?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "cwcud");?></p>

        <div class="inner_form">            
            <label for="user_pwd"><?php _e('Confirmation du mot de passe *', 'aosc'); ?></label>
            <input type="password" id="user_pwd_confirm" name="user_pwd_confirm" placeholder="" required>
        </div>


        <p class="form-info form-item form-legal-text"><?php _e('* Champs obligatoires', 'aosc'); ?></p>
        <div class="btn_container">
            <button class="button form-item"  type="submit" id="sign-in-coworker-btn"><?php _e('Enregistrer', 'aosc'); ?></button>
        </div>

		<div id="sign-in-coworker-btn-send-ok" style="display:none;">
			<?php _e('Votre demande a bien été envoyée.','aosc');?>
		</div>

    </form>
	<?php
endwhile; // End of the loop.

get_footer();
