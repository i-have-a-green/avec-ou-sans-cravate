<?php
/*
Template Name: logout
*/
wp_logout();
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
?>
<div class="aligndefault login">
	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>	
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
<?php
	$redirect = get_permalink(get_field("dashboard-coworker", "options"));

	wp_login_form(
		array(
			'remember'       => false,
			'label_username' => __( 'Adresse email', 'aosc' ),
			'label_password' => __( 'Mot de passe', 'aosc' ),
			'redirect'       => $redirect
		)
	);?>
	<a class="lost-pswd" href="<?php echo wp_lostpassword_url(get_home_url()) ; ?>"><?php _e("Mot de passe oublié ?", "aosc"); ?></a>
</div>
	<?php
endwhile; // End of the loop.

get_footer();
