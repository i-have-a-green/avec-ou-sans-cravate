<?php
/*
Template Name: sos search
*/

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>
	<!--<form action="" method="post"  name="sos-search-form" id="sos-search-form" class="form-style alignwide">
		<section id="section-user" >
			<div class="centering_wrapper">
				<h2><?php _e("Vous cherchez un lieu pour votre événement", 'aosc');?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<p><b><?php _e("Gagnez du temps en nous confiant votre recherche gratuitement !","aosc");?></b></p>
				<div class="inner_section">
					<input type="hidden" name="honeyPot" value="">
					<div class="inner_form">
						<label for="user_lastname"><?php _e('Nom *', 'aosc'); ?></label>
						<input type="text" id="user_lastname" name="user_lastname" placeholder="Dubois" required>
					</div>
		
					<div class="inner_form">
						<label for="user_firstname"><?php _e('Prénom *', 'aosc'); ?></label>
						<input type="text" id="user_firstname" name="user_firstname" placeholder="Véronique" required>
					</div>
		
					<div class="inner_form">
						<label for="user_email"><?php _e('Adresse mail *', 'aosc'); ?></label>
						<input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required>
					</div>
					
					<div class="inner_form">
						<label for="user_phone"><?php _e('Téléphone *', 'aosc'); ?></label>
						<input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04">
					</div>
					<div class="inner_form">
						<label for="user_date"><?php _e('Date désirée', 'aosc'); ?></label>
						<input type="tel" id="user_date" name="user_date" placeholder="" >
					</div>
					
					
					<div class="btn_container">
						<a class="button" href="#section-what"><?php _e('Étape suivante', 'aosc'); ?></a>
					</div>
				</div>
			</div>
			
		</section>

		<section id="section-what">
			<div class="centering_wrapper">
				<h2><?php _e("Votre besoin ? ", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="custom_need_form">
					<input type="hidden" name="ask_what" id="ask_what" value="" required>
					<span onclick="change_ask_what(this, 'travailler');" class="ask_what"><?php _e("Travailler mes dossiers hors de chez moi", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'afterwoork');" class="ask_what"><?php _e("Réalisez un afterwoork/soirée d'entreprise/pot de départ", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'séminaire');" class="ask_what"><?php _e("Organiser un séminaire", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'autre');" class="ask_what"><?php _e("Autre", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'rdv');" class="ask_what"><?php _e("Recevoir un(des) client(s) / partenaire(s) en rendez-vous", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'réunion');" class="ask_what"><?php _e("Organiser une réunion de travail", 'aosc');?></span>
					<span onclick="change_ask_what(this, 'bien être');" class="ask_what"><?php _e("Activité de bien être", 'aosc');?></span>
				</div>
				<div class="btn_container">
					<a class="button" href="#section-user"><?php _e('Étape précédente', 'aosc'); ?></a>
					<a class="button" href="#section-how-many"><?php _e('Étape suivante', 'aosc'); ?></a>
				</div>
			</div>
		</section>

		<section id="section-how-many">
			<div class="centering_wrapper">
				<h2><?php _e("Nombre de participants ? ", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="inner_section">
					<div class="inner_form">
						<label for="ask_participants_number"><?php _e("Nombre de personnes", 'aosc');?></label>
						<input type="number" min="1" name="ask_participants_number" id="ask_participants_number">
					</div>
				</div>
	
				<h2><?php _e("Des précisions à apporter ? ", 'aosc'); ?></h2>
				<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">
				<div class="inner_section">
					<div class="inner_form">
						<label for="ask_comment"><?php _e("Entrez ici vos remarqes", 'aosc');?></label>
						<textarea name="ask_comment" id="ask_comment" placeholder="Tapez votre message"></textarea>
					</div>
				</div>
				<div class="btn_container">
					<a class="button" href="#section-what"><?php _e('Étape précédente', 'aosc'); ?></a>
					<input type="submit" class="button" id="sos-search-form-btn" value="<?php _e('Envoyez votre demande', 'aosc'); ?>">
				</div>
				<h3 style="display:none;" id="sos-search-form-ok">
					<?php _e('Votre demande a bien été envoyée.','aosc');?>
				</h3>
			</div>
		</section>
	</form>-->

	Commun à ask property

	<?php
endwhile; // End of the loop.

get_footer();
