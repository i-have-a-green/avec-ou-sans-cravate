<?php
/*
Template Name: owner-incoming-contact
*/
$user = secure_role('owner');
$comments = array();
$properties = get_posts( array( 
	'numberposts' => -1,
	'author' 	=> $user->ID,
	'post_type'	=> 'property',
	) 
);

$nbr_property = count($properties);
$comments_count = 0;
$tab_properties_id = array();
if($nbr_property > 0){
	foreach($properties as $property){
		$tab_properties_id[] = $property->ID;
	}
	$args = array(
		'post__in' => $tab_properties_id,   
		'orderby'	=> 'comment_date',
		'order'	=> 'ASC',
	);
	if(isset($_GET['filter_property']) && !empty($_GET['filter_property'])){
		$args['post__in'] = array($_GET['filter_property']);
	}
	if(isset($_GET['sort_property']) && $_GET['sort_property'] == 'ask_date'){
		$args['meta_key'] = 'ask_date';
		$args['orderby'] = 'meta_value';
		$args['order'] = 'ASC';
	}
	$comments = get_comments( $args );
}


get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="owner-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-owner' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<form class="owner-contact" action="<?php the_permalink();?>" method="GET">
		<div class="inner_form">
			<div class="select">
				<label for="filter_property"><?php _e('Filtrer', 'aosc'); ?></label>
				<select name="filter_property" id="filter_property">
					<option value="">Tous mes espaces</option>
					<?php
						foreach($tab_properties_id as $property_id){
							echo '<option value="'.$property_id.'" '.selected($property_id, $_GET['filter_property']).'>'.get_the_title($property_id).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="inner_form">
			<div class="select">
				<label for="sort_property"><?php _e('Trier', 'aosc'); ?></label>
				<select name="sort_property" id="sort_property">
					<option value="comment_date" <?php selected('comment_date', $_GET['sort_property']);?>>Date d'émission</option>
					<option value="ask_date" <?php selected('ask_date', $_GET['sort_property']);?>>Date de location</option>
				</select>
			</div>
		</div>
		<div class="inner_form submit-image">
			<input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/images/Loupe.png" name="clicImage"/>
		</div>
	</form>

	<div class="last-ask-list">
		<?php 
		for($i=0;$i<sizeof($comments);$i++){
			set_query_var( 'comment', $comments[$i] );
			get_template_part( 'template-parts/content/content-comment' );
		}

		
		?>
	</div>
</div>

<?php
endwhile; // End of the loop.

get_footer();
