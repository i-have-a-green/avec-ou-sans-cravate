<?php
/*
Template Name: owner-property
*/
$user = secure_role('owner');
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

<div class="owner-global aligndefault">

	<div class="menu">
		<?php get_template_part( 'template-parts/menu/menu-owner' ); ?>
	</div>

	<?php the_title( '<h2 class="entry-title aligncenter">', '</h2>' ); ?>
	<hr class="wp-block-separator aligncenter has-text-color has-background has-orange-background-color has-orange-color">

	<!-- <div class="owner-properties-grid"> -->
		
		<?php 
			$properties = get_posts(array(
				'post_type' 	=> 'property',
				'author'		=> $user->ID,
				'numberposts'	=> -1
			));
			global $post;
			if (sizeof($properties) == 0) {
				?>
					<h3>Proposez dès maintenant votre premier espace de travail dans l'onglet "Ajouter un bien"</h3>
				<?php
			}
			else {
				?>
				<div class="owner-properties-grid">
					<?php
				foreach ($properties as $post){
					setup_postdata( $post );
					get_template_part( 'template-parts/content/content', 'owner-property' ); 
				}
				wp_reset_postdata();
				?></div>
				<?php
			}
		?>
	<!-- </div> -->

</div>
<?php
endwhile; // End of the loop.

get_footer();
