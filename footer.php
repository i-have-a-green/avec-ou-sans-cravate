<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

				<div id="modal_like" class="" onclick="document.getElementById('modal_like').style.display='none'">
					<div class="inner_modal_container">
						<div class="inner_modal">
							<span onclick="document.getElementById('modal_like').style.display='none'">&times;</span>
							<h4>Connectez vous pour ajouter ce bien à vos favoris</h4>
							<a class="button" href="<?php echo get_permalink(get_field('connect', 'option'));?>">Se connecter</a>
						</div>
					</div>
				</div>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

	
	<?php /* if ( is_active_sidebar( 'pre-footer' ) ) : ?>

	<div class="pre-footer">
		<?php dynamic_sidebar( 'pre-footer' ); ?>
	</div><!-- .widget-area -->

	<?php endif;*/ ?>
	<footer id="colophon" class="site-footer" role="contentinfo">

	
		<div class="site-info">
			
			<?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
				<aside class="">
					<?php dynamic_sidebar( 'sidebar-footer' ); ?>
				</aside><!-- .widget-area -->
			<?php endif; ?>


		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
