<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
include_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php';
include_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php';

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post(); 
	$ask = add_query_arg( array(
			'id_property' => get_the_id()
		), 
		get_permalink(get_field("ask-property", "options") ) 
	);
	?>
	
	<div class="single_main-content alignwide">
		<div class="cols">
			<div class="col thumbnail">
				<div id="slider">
					<?php
						$size = '750-400';
						the_post_thumbnail( $size );
						for($img_gallery_nbr=1; $img_gallery_nbr<=5; $img_gallery_nbr++){
							img_gallery('img-gallery-'.$img_gallery_nbr,$size);
						}
					?>
				</div>
				<div class="slider-nav">
					
				</div>
				<a href="<?php echo $ask;?>" class="button info_ask"><?php _e("Contacter l'hôte", 'aosc');?></a>
			</div>
			<div class="col content">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="has-orange-color under_title">
					<?php 
					$during = get_field('during');
					if($during == 'hour'){$during = "L’heure";}
					else if($during == 'hours'){$during = "Les deux heures";}
					else if($during == 'half-day'){$during = "La demi-journée";}
					else if($during == 'day'){$during = "La journée";}
					else if($during == 'week'){$during = "La semaine";}

					if(get_field('price') > 0):
						echo $during; ?> à partir de <?php the_field('price'); ?> €
					<?php else:
						echo 'Gratuit';
					endif;?>	
				</p>
				<?php
				$display_area = false;
				$terms =  (array)get_the_terms(get_the_id(), "type");
				if($terms){
					foreach($terms as $term){
						if($term->slug == "salle-de-reunion" || $term->slug == "bureau-privatif"){
							$display_area = true;
						}
					}
				}
				if($display_area):
				?>
				<p>
				Superficie : <?php the_field('area'); ?>	m²
				</p>
				<?php endif;?>

				<?php the_field('address'); ?>
				
				<p>
					<?php the_field('zip_code'); ?>
					<?php the_field('city'); ?>
				</p>	
			
				<h2><?php _e('Contact','aosc');?></h2>
			
				<?php the_author_meta( 'user_firstname' , $post->post_author); ?> 
				<?php the_author_meta( 'user_lastname' , $post->post_author); ?> 
			
				<span class="button" data-phone="<?php the_field( 'user_phone' , 'user_'.$post->post_author); ?>" onClick="this.innerHTML = this.dataset.phone;">
					<?php _e('Voir le numéro de téléphone','');?>
				</span>
				
				<h2><?php _e('À propos de cet espace de travail','aosc');?></h2>
			
				<?php the_content();?>
			</div>
		</div>
	</div>
	<?php
	$terms = get_the_terms($post->ID, "service");
	if(isset($terms[0])):?>
		<div class="services_container">
			<h2 class="alignwide"><?php _e('Les services','aosc');?></h2>
			<div class="services_pictos alignwide">
			<?php
				foreach($terms as $term){
					$image = get_field('image', 'service_'.$term->term_id);
					?>
					<div class="single_picto">
						<?php
						if( $image ) {
							$upload_dir = wp_upload_dir();
							$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
							$wp_filesystem = new WP_Filesystem_Direct(null);
							$svg = $wp_filesystem->get_contents($svgpath);
							echo $svg;
						}
						echo $term->name;
					?>
					</div>
					<?php
				}
			?>
			</div>
		</div>
	<?php endif;?>
	<div class="alignwide">
		<?php if(!empty(get_field('access'))):?>
			<h2><?php _e("Accès et modalités d'arrivée",'aosc');?></h2>
			<?php the_field('access'); ?>
		<?php endif;?>

		<?php if(!empty(get_the_author_meta( 'description' , $post->post_author))):?>
			<h2><?php _e("Description de l'entreprise",'aosc');?></h2>
			<?php echo nl2br(get_the_author_meta( 'description' , $post->post_author)); ?> 
		<?php endif;?>
	</div>
	<p><a href="<?php echo $ask;?>" class="button info_ask"><?php _e("Contacter l'hôte", 'aosc');?></a></p>
	<?php
	$args = array(
		'post_type' 	=> 'property',
		'author'        =>  $post->post_author, 
		'orderby'       =>  'rand',
		'posts_per_page'=> 3,
		'post__not_in'	=> array( $post->ID )
	);
	
	$properties = get_posts( $args );
	if(sizeof($properties) > 0):?>
		<div class="aligndefault">
			<h2 class="classic-h2"><?php _e("Nos autres espaces disponibles",'aosc');?></h2>
			<hr class="wp-block-separator has-text-color has-background has-orange-background-color has-orange-color" style="margin: 2rem auto">
			<div class="property_grid">
					<?php
					foreach($properties as $post){
						setup_postdata( $post );
						get_template_part( 'template-parts/content/content', 'property' );
					}
					wp_reset_postdata();
					?>
			</div>
		</div>
	<?php endif;?>

</div>

<?php
endwhile; // End of the loop.

get_footer();

function img_gallery($img, $size){
	$image = get_field($img);
	if( $image ) {
		echo wp_get_attachment_image( $image, $size );
	}
}