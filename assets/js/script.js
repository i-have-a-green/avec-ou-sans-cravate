document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("wpcf7-form");
    Array.prototype.forEach.call(els, function(el) {
        el.removeAttribute("novalidate");
    });
});


function initMap() {
    var markers = [];
    macarte = L.map(elemMap).setView([lat, lon], zoom);
    markerClusters = L.markerClusterGroup();
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);

    /*var myIconTarget = L.icon({
        iconUrl: iconBase + "target.png",
        iconSize: [32, 32],
        iconAnchor: [16, 16],
        //popupAnchor: [-0, -42],
    });*/
    lat = parseFloat(lat);
    lon = parseFloat(lon);
    //var marker = L.marker([lat, lon], { icon: myIconTarget }).addTo(macarte);

    for (property in tab_properties) {        

        coordinates = tab_properties[property].coordonate.split(" ");

        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);
        if(tab_properties[property].picto){
            // Nous définissons l'icône à utiliser pour le marqueur, sa taille affichée (iconSize), sa position (iconAnchor) et le décalage de son ancrage (popupAnchor)
            var myIconTarget = L.icon({
                iconUrl: tab_properties[property].picto,
                iconSize: [35, 59],
                iconAnchor: [17, 59],
                popupAnchor: [0, -56],
            });
            var marker = L.marker([lat, lon], { icon: myIconTarget }).addTo(macarte);
        }
        else{
            var marker = L.marker([lat, lon]).addTo(macarte);
        }
        marker.bindPopup(tab_properties[property].name);
        markerClusters.addLayer(marker);
        markers.push(marker);
    }
    //macarte.addLayer(markerClusters);
    var group = new L.featureGroup(markers); // Nous créons le groupe des marqueurs pour adapter le zoom
	macarte.fitBounds(group.getBounds().pad(0.1)); // Nous demandons à ce que tous les marqueurs soient visibles, et ajoutons un padding (pad(0.5)) pour que les marqueurs ne soient pas coupés
	macarte.addLayer(markerClusters);
}

if (document.getElementById("mapProperties")) {
    elemMap = "mapProperties";
    initMap();
}

function mapOpen() {
    if (document.querySelector("#mapProperties")) {
        //initMap();
        //Utiliser get ID
        document.querySelector(".map_container").classList.toggle('active')
        document.querySelector(".property_grid").classList.toggle('map_open')
        document.querySelector(".property_form").classList.toggle('map_open')
        document.querySelector(".mapOpener").classList.toggle('map_open')
        if (document.querySelector(".mapOpener").classList.contains('map_open')) {
            setCookie('map', 1, 1);
            document.querySelector(".mapOpener").innerHTML="Fermer"
        }
        else{
            document.querySelector(".mapOpener").innerHTML="Voir la carte"
            setCookie('map', 0, 1);
        }
    }
}
function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

function change_ask_what(elem, value){
    var els = document.getElementsByClassName("ask_what");
    Array.prototype.forEach.call(els, function(el) {
        el.classList.remove('active');
    });
    elem.classList.add('active');
    document.getElementById('ask_what').value = value;
}

if(document.getElementById("ask_property_form")){

    document.forms.namedItem("ask_property_form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('ask_property_submit_button').disabled = true;
        var form = document.forms.namedItem("ask_property_form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'ask_property', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('ask_property_submit_button').disabled = true;
                document.getElementById('ask_property_send_ok').style.display = 'block';
                setTimeout(function(){
                    location.href = document.getElementById('redirect').value; 
                }, 1500);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("sign-in-coworker-form")){
    document.forms.namedItem("sign-in-coworker-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('sign-in-coworker-btn').disabled = true;
        var form = document.forms.namedItem("sign-in-coworker-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'sign-in-coworker', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('sign-in-coworker-btn').disabled = true;
                document.getElementById('sign-in-coworker-btn-send-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = document.getElementById('redirect').value; 
                }, 1500);
            }
            if (xhr.status === 304) {
                console.log(xhr.response);
                document.getElementById('sign-in-coworker-btn').disabled = true;
                document.getElementById('sign-in-coworker-btn-send-ok').innerHTML= 'Adresse email existante, veuillez vous connecter';
                document.getElementById('sign-in-coworker-btn-send-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = login_url; 
                }, 1500);
            }
        };
        xhr.send(formData);
    });

}
if(document.getElementById("user_pwd") && document.getElementById("user_pwd_confirm")){
    var password = document.getElementById("user_pwd"), confirm_password = document.getElementById("user_pwd_confirm");
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
}

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Les mots de passe ne correspondent pas");
    } else {
        confirm_password.setCustomValidity('');
    }
}
    

if(document.getElementById("owner-profile-form")){
    document.forms.namedItem("owner-profile-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('owner-profile-form-btn').disabled = true;
        var form = document.forms.namedItem("owner-profile-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'owner-profile', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('owner-profile-form-btn').disabled = true;
                document.getElementById('owner-profile-form-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = dashboardOwner;
               }, 1500);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("coworker-profile-form")){
    document.forms.namedItem("coworker-profile-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('coworker-profile-form-btn').disabled = true;
        var form = document.forms.namedItem("coworker-profile-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'coworker-profile', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('coworker-profile-form-btn').disabled = true;
                document.getElementById('coworker-profile-form-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = dashboardCoworker;
               }, 1500);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("pre-owner-form")){
    document.forms.namedItem("pre-owner-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('pre-owner-form-btn').disabled = true;
        var form = document.forms.namedItem("pre-owner-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'pre-owner', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('pre-owner-form-btn').disabled = true;
                location.href = dashboardOwner;
            }
            if (xhr.status === 304) {
                console.log(xhr.response);
                document.getElementById('pre-owner-form-btn').disabled = true;
                document.getElementById('pre-owner-form-ok').innerHTML= 'Adresse email existante, veuillez vous connecter';
                document.getElementById('pre-owner-form-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = login_url; 
                }, 2500);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("owner-company-form")){
    document.forms.namedItem("owner-company-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('owner-company-form-btn').disabled = true;
        var form = document.forms.namedItem("owner-company-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'owner-company', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('owner-company-form-btn').disabled = true;
                document.getElementById('owner-company-form-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = dashboardOwner; 
               }, 1500);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("coworker-company-form")){
    document.forms.namedItem("coworker-company-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('coworker-company-form-btn').disabled = true;
        var form = document.forms.namedItem("coworker-company-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'coworker-company', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('coworker-company-form-btn').disabled = true;
                document.getElementById('coworker-company-form-ok').style.display = 'block';
                setTimeout(function(){
                    location.href = dashboardCoworker;
               }, 1500);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("owner-add-property-form")){
    document.forms.namedItem("owner-add-property-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('owner-add-property-form-btn').disabled = true;
        document.getElementById('owner-add-property-form-btn').value = "En cours de traitement";
        document.getElementById('time-machine').classList.add('active');
        var form = document.forms.namedItem("owner-add-property-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'owner-add-property', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('owner-add-property-form-btn').disabled = true;
                document.getElementById('owner-add-property-form-ok').style.display = 'block';
                document.getElementById('time-machine').classList.remove('active');
                document.getElementById('owner-add-property-form-btn').value = "Valider les informations";
                console.log(xhr.response);
                setTimeout(function(){
                    location.href = dashboardOwner;
               }, 5000);
            }
        };
        xhr.send(formData);
    });
}

if(document.getElementById("sos-search-form")){
    document.forms.namedItem("sos-search-form").addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        document.getElementById('sos-search-form-btn').disabled = true;
        var form = document.forms.namedItem("sos-search-form");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'sos-search', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sos-search-form-btn').disabled = true;
                document.getElementById('sos-search-form-ok').style.display = 'block';
            }
        };
        xhr.send(formData);
    });
}


if (document.getElementById('ask-property-ariane')) {
    let svg = document.getElementById('ask-property-ariane')


    document.addEventListener('scroll',function () {

        if (document.getElementById('section-user').offsetTop-180 <= window.scrollY) {
            svg.classList.add('activeG1')
        }
        else{
            svg.classList.remove('activeG1')
        }

        if (document.getElementById('section-when').offsetTop-180 <= window.scrollY) {
            svg.classList.add('activeG2')
        }
        else{
            svg.classList.remove('activeG2')
        }

        if (document.getElementById('section-what').offsetTop-180 <= window.scrollY) {
            svg.classList.add('activeG3')
        }
        else{
            svg.classList.remove('activeG3')
        }

        if (document.getElementById('section-how-many').offsetTop-180 <= window.scrollY) {
            svg.classList.add('activeG4')
        }
        else{
            svg.classList.remove('activeG4')
        }
    })
}
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("wish_btn_not_connect");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(){
            document.getElementById('modal_like').style.display='block'
        });
    });
});

document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("wish_btn_connect");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(){
            el.parentNode.classList.toggle("like");
            var formData = new FormData(); //fonction native qui mets tout dans un format prêt a etre envoyé
            formData.append("id_property", el.dataset.idProperty);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'manage-wishlist', true);
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            xhr.onload = function() {
                if (xhr.status === 200) {
                   /* console.log(xhr.response); */
                }
            };
            xhr.send(formData);
        });
    });
});





if (document.getElementById("pwdView")) {
    document.getElementById("pwdView").addEventListener('click',function () {
        let x = document.getElementById('user_pwd')
        if (x.type === "password") {
            x.type = "text";
            document.getElementById("pwdView").innerText = "Cacher le mot de passe."
        } else {
            x.type = "password";
            document.getElementById("pwdView").innerText = "Voir le mot de passe."
        }
    })
}
if (document.getElementById("add_leave_date_btn")) {
    document.getElementById("add_leave_date_btn").addEventListener('click',function (e) {
        e.preventDefault();
        e.stopPropagation();
        var date_start = new Date (document.getElementById("date_start").value);
        var date_end = new Date (document.getElementById("date_end").value);
        //console.log(date_end.toLocaleDateString("fr-FR"));

        var leave_date = document.getElementById("leave_date");
        try {
            var tab = JSON.parse(leave_date.value); 
        } catch (e) {
            var tab = new Array();
        }

        if(date_end.getTime() > date_start.getTime()){
            tab.push( new Array(date_start.getTime(), date_end.getTime()));
        }
        else{
            tab.push(date_start.getTime());
        }

        document.getElementById("leave_date").value = JSON.stringify(tab);
        display_leave_date();
    });
}

function verif_date(){
    var date_start = new Date (document.getElementById("date_start").value);
    var date_end = new Date (document.getElementById("date_end").value);
    if(date_start.getTime() > date_end.getTime()){
        document.getElementById("date_end").value = document.getElementById("date_start").value;
    }
    document.getElementById("date_end").setAttribute("min", document.getElementById("date_start").value);
}

document.addEventListener('DOMContentLoaded', function() {    display_leave_date(); });
function display_leave_date(){
    if (document.getElementById("leave_date")) {
        var leave_date = document.getElementById("leave_date");
        try {
            var tab = JSON.parse(leave_date.value); 
        } catch (e) {
            var tab = new Array();
        }
        var leave_date_display = '';
        tab.forEach(function(item, index) {
            if(Array.isArray(item)){
                date_start = new Date (item[0]);
                date_end = new Date (item[1]);
                leave_date_display += '<span class="button">Du '+date_start.toLocaleDateString("fr-FR")+' au '+date_end.toLocaleDateString("fr-FR")+' <span class="remove" onclick="remove_date_leave('+index+')">✗</span></span>';
            }else{
                date_start = new Date (item);
                leave_date_display += '<span class="button">Le '+date_start.toLocaleDateString("fr-FR")+' <span class="remove" onclick="remove_date_leave('+index+')">✗</span></span>';
            }
        });
        document.getElementById("leave_date_display").innerHTML = leave_date_display;
    }
}

function remove_date_leave(index){
    var leave_date = document.getElementById("leave_date");
    var tab = JSON.parse(leave_date.value); 
    var tabTemp = new Array();
    tab.forEach(function(item, indextab) {
        if (index !== indextab){
            tabTemp.push(item);
        }
    });
    document.getElementById("leave_date").value = JSON.stringify(tabTemp);
    display_leave_date();
}


/* if (document.querySelectorAll('input')) {
    document.querySelectorAll('input').forEach(el => {
        if (el.hasAttribute('required')) {
            const validityState = el.validity;
            if (validityState.valueMissing) {
                el.setCustomValidity('Champs requis');
            }else {
                el.setCustomValidity('');
            }
            el.reportValidity();
        }
    });
} */

