jQuery( document ).ready( function( $ ) {

    $('#slider').slick({
        dots: true,
        appendDots: $('.slider-nav'),
        customPaging: function(slider, i) {
            // this example would render "tabs" with titles
            return '<span class="dot"></span>';
        },
        prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button" style="">←</button>',
        nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="">→</button>',
    });

    /* $('#slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#slider-nav',
    });
    $('#slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '#slider-for',
        infinite: true,
        centerMode: true,
        focusOnSelect: true,
        prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button" style="">←</button>',
        nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="">→</button>',
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true
              }
            },{
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 500,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 350,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
    }); */
});

