<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_property');
// let's create the function for the custom type
function custom_post_property() {
	

	register_post_type( 'property', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Biens', 'ihag'), /* This is the Title of the Group */
				'singular_name' 	=> __('Bien', 'ihag'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-sticky', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'nos-espaces' ),
			'show_in_rest'		=> false,
			'has_archive' => true,
			'supports' 			=> array( 'title', 'editor', 'thumbnail', 'revisions', 'author', 'comments'),
			'capabilities' => array(
				'edit_post'          => 'edit_property', 
				'read_post'          => 'read_property', 
				'delete_post'        => 'delete_property', 
				'delete_posts'        => 'delete_properties', 
				'edit_posts'         => 'edit_properties', 
				'edit_others_posts'  => 'edit_others_properties', 
				'publish_posts'      => 'publish_properties',       
				'read_private_posts' => 'read_private_properties', 
				'create_posts'       => 'edit_properties', 
			  ),
			"capability_type" => array( 'property','properties' ),
			"map_meta_cap" => true,

	 	) /* end of options */
	);

	

	$args = array(
        'label'        => __( 'Type', 'ihag' ),
        'hierarchical' => true
    );
    register_taxonomy( 'type', 'property', $args );

	$args = array(
        'label'        => __( 'Service', 'ihag' ),
        'hierarchical' => true
    );
    register_taxonomy( 'service', 'property', $args );

	$args = array(
        'label'        => __( 'Disponibilité', 'ihag' ),
        'hierarchical' => true
    );
    register_taxonomy( 'availability', 'property', $args );

	
	
}


function ihag_no_admin_access()
{
    if ( !is_admin() || (is_user_logged_in() && isset( $GLOBALS['pagenow'] ) AND 'wp-login.php' === $GLOBALS['pagenow'] ) ) {
        return;
    }

	$redirect = esc_url(home_url());
	$user = wp_get_current_user();
	$user_roles = $user->roles;
	if ( in_array( 'owner', $user_roles, true ) ){
		wp_logout();
		exit( wp_redirect( $redirect ) );
	}
}
add_action( 'admin_init', 'ihag_no_admin_access', 100 );

