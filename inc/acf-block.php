<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'AOSC',
                'title' => __( 'Custom', 'ihag' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        // Basic Blocks

        acf_register_block_type(
            array(
                'name'				    => 'fomr_home',
                'title'				    => __('Formulaire accueil'),
                'render_template'	    => 'template-parts/block/form_home.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'admin-page',
                'keywords'			    => array('form', 'ihag'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'home_properties',
                'title'				    => __('3 biens'),
                'render_template'	    => 'template-parts/block/home-properties.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'admin-page',
                'keywords'			    => array('home', ''),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
            )
        );

    }
}