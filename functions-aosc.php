<?php
add_action('init', 'ihag_register_role' );
function ihag_register_role() {
	add_role( 'coworker', __( 'Coworker	', 'aosc' ), array( 'read' => true, 'level_0' => true ) );
	add_role( 'owner', __( 'Bailleur', 'aosc' ), array (
        'edit_property' => true,
        'read_property' => true,
        'delete_property' => true,
        'edit_properties' => true,
        'edit_others_properties' => true,
        'publish_properties' => true,
        'read_private_properties' => true,
        'edit_properties' => true,
        
    ) );
	//add_role( 'preowner', __( 'Bailleur à valider', 'aosc' ), array( 'read' => true, 'level_0' => true ) );
}

function wporg_simple_role_caps() {
    // Gets the simple_role role object.
    $role = get_role( 'administrator' );
    $role->add_cap( 'edit_property', true );
	$role->add_cap( 'read_property', true );
	$role->add_cap( 'delete_property', true );
	$role->add_cap( 'delete_properties', true );
	$role->add_cap( 'edit_properties', true );
	$role->add_cap( 'edit_private_posts', true );
	$role->add_cap( 'edit_others_properties', true );
	$role->add_cap( 'publish_properties', true );
	$role->add_cap( 'read_private_properties', true );
	$role->add_cap( 'edit_published_properties', true );

	$role = get_role( 'owner' );
    $role->add_cap( 'edit_property', true );
	$role->add_cap( 'read_property', true );
	$role->add_cap( 'delete_property', true );
	$role->add_cap( 'edit_properties', true );
	$role->add_cap( 'edit_private_posts', true );
	$role->add_cap( 'edit_others_properties', true );
	$role->add_cap( 'publish_properties', true );
	$role->add_cap( 'read_private_properties', true );
	$role->add_cap( 'edit_published_properties', true );
}

add_filter('wp_dropdown_users', 'test');
function test($output) 
{
	global $post;
	
	//Doing it only for the custom post type
	if($post->post_type == 'property')
	{
		$output = "<select id='post_author_override' name='post_author_override' class=''>";
		$users = get_users(array('role'=>'administrator'));
		foreach($users as $user)
		{
			$output .= "<option value='".$user->id."' ".selected($user->id, $post->post_author, false).">".$user->user_login."</option>";
		}
		$users = get_users(array('role'=>'owner'));
		foreach($users as $user)
		{
			$output .= "<option value='".$user->id."' ".selected($user->id, $post->post_author, false).">".$user->user_login."</option>";
		}
		$output .= "</select>";
	}
	return $output;
}
 
// Add simple_role capabilities, priority must be after the initial role definition.
add_action( 'init', 'wporg_simple_role_caps', 11 );
function check_nonce(){
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}
function save_property_gps( $post_id, $post, $update )  {

	// Sinon la fonction se lance dès le clic sur "ajouter"
	if( ! $update ) {
		return;
	}

	// On ne veut pas executer le code lorsque c'est une révision
	if( wp_is_post_revision( $post_id ) ) {
		return;
	}

	// On évite les sauvegardes automatiques
	if( defined( 'DOING_AUTOSAVE' ) and DOING_AUTOSAVE ) {
		return;
	}

	// Seulement pour les articles
	if( $post->post_type == 'property' ) {

		//clean availability
		$terms = get_the_terms($post_id, "availability");
		$tab_availability = array();
		if($terms){
			foreach($terms as $term){
				$tab_availability[] = $term->slug;
				if($term->parent != '0'){
					$term_parent = get_term_by( 'id', $term->parent, 'availability');
					if(!in_array($term_parent->slug, $tab_availability)){
						$tab_availability[] = $term_parent->slug;
					}
				}
			}
			wp_set_object_terms( $post_id, $tab_availability, 'availability' );
		}
		

		//coordinate
		/*$ch = curl_init();
		$url = "https://api-adresse.data.gouv.fr/search/?q=".str_replace(" ", "+", get_field('address',$post->ID).' '.get_field('city',$post->ID)).'&postcode='.trim(get_field('zip_code',$post->ID))."&lat=46.974&lon=-1.2925";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);	
		$r = curl_exec($ch);
		$info = curl_getinfo($ch);*/
		
		//$client = new Client(); // GuzzleHttp\Client
		$baseURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
		$addressURL = urlencode(get_field('address',$post->ID) . ' '.trim(get_field('zip_code',$post->ID)).' ,' . urlencode(get_field('city',$post->ID)) . ', France') . '&key='.get_field('cle_api_google_geocode', 'option');
		$url = $baseURL . $addressURL;
		/*$json = file_get_contents($url);
		$response = json_decode($json);*/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);	
		$response = json_decode( curl_exec($ch) );
		
		
		curl_close($ch);
		
		//$request = $client->request('GET', $url);
		//$response = $request->getBody()->getContents();
		//$response = json_decode($response);
		if(isset($response->results[0])){

			error_log(json_encode($response->results[0]));
			/*$r = json_decode( $r );
			$address = $r->features[0]->properties->label;
			$r = $r->features[0]->geometry->coordinates[1].' '.$r->features[0]->geometry->coordinates[0] ;
			
			curl_close($ch);
			*/
			$lat = $response->results[0]->geometry->location->lat;
			$lon = $response->results[0]->geometry->location->lng;
			$r = $lat.' '.$lon;
			$address = addslashes($response->results[0]->formatted_address);

			global $wpdb;
			$tablename = $wpdb->prefix.'properties_gps';
	
			$sql = $wpdb->get_results("SELECT * FROM `$tablename` WHERE post_id = $post->ID");
			if($wpdb->num_rows > 0){
				$sql = $wpdb->prepare("DELETE FROM `$tablename` WHERE post_id = $post_id ");
				$wpdb->query($sql);	
			}
			//$sql = $wpdb->prepare("INSERT INTO `$tablename` (`post_id`, `coordinate`, `address`) values ($post->ID, GeomFromText('POINT($r)'), '$address' ) ");
			$sql = $wpdb->prepare("INSERT INTO `$tablename` (`post_id`, `coordinate`, `address`) values ($post->ID, ST_GeomFromText('POINT($r)'), '$address' ) ");
			$wpdb->query($sql);
			
			update_post_meta($post_id, 'gps', $r);
			update_post_meta($post_id, 'gps_address', $address);
	
		}
		else{
			global $wpdb;
			$tablename = $wpdb->prefix.'properties_gps';
	
			$sql = $wpdb->get_results("SELECT * FROM `$tablename` WHERE post_id = $post->ID");
			if($wpdb->num_rows > 0){
				$sql = $wpdb->prepare("DELETE FROM `$tablename` WHERE post_id = $post_id ");
				$wpdb->query($sql);
			}
			/*if($wpdb->num_rows > 0){
				$sql = $wpdb->prepare("UPDATE `$tablename` SET coordinate = NULL, address = '' WHERE post_id = $post_id ");
				$wpdb->query($sql);
			}else{
				$sql = $wpdb->prepare("INSERT INTO `$tablename` (`post_id`, `coordinate`, `address`) values ($post->ID, NULL, '') ");
				$wpdb->query($sql);
			}*/
			update_post_meta($post_id, 'gps', '');
			update_post_meta($post_id, 'gps_address', '');
		}
		add_filter( 'redirect_post_location', 'add_notice_query_var', 99, 2 );
		return;
	}

}
add_action( 'save_post', 'save_property_gps', 10, 3 );

function add_notice_query_var( $location, $post_id ) {
	remove_filter( 'redirect_post_location', 'add_notice_query_var', 99 );
	return add_query_arg( array( 'coordinate' => $post_id ), $location );
   }

add_action( 'admin_notices', 'admin_notices' );
function admin_notices() {
	if ( ! isset( $_GET['coordinate'] ) ) {
	  return;
	}
	$address = get_post_meta( $_GET['coordinate'], 'gps_address', true );
	if(empty($address)):
	?>
	<div class="notice notice-error">
		<p><?php _e( 'Le positionnement géographique de ce bien n\'a pas fonctionné, veuiller modifier l\'adresse' ); ?></p>
	</div>
	<?php
	else:
	?>
	<div class="notice notice-success">
		<p><?php _e( 'Le positionnement géographique de ce bien est : '.$address.' ('.get_post_meta( $_GET['coordinate'], 'gps', true ).')' ); ?></p>
	</div>
	<?php
	endif;
   }

function ihag_query_property( $wp_query ) {


	global $lon, $lat;
	if(is_admin()){
		return;
	}

	if ( $wp_query->is_main_query() && $wp_query->get( 'post_type' ) === 'property' && isset($_POST['action']) && $_POST['action'] == 'search_property' ) {

		if ( ! $tax_query = $wp_query->get( 'tax_query' ) ){
			$tax_query = array('relation' => 'AND');
		}
		if ( isset($_POST['taxo_type']) && !empty($_POST['taxo_type']) ) {
			$tax_query[] = array(
				'taxonomy' => 'type', 
				'field' => 'term_id',
				'terms' => array( sanitize_text_field( $_POST['taxo_type'] ) ),
			);
		}
		if ( isset($_POST['date_availability']) && !empty($_POST['date_availability']) ) {
			$date_slug =  strtolower(date("l", strtotime($_POST['date_availability'])));
			$tax_query[] = array(
				'taxonomy' => 'availability', 
				'field' => 'slug',	
				'terms' => $date_slug,
			);

			$properties_date = get_posts(array(
				'post_type'	=> 'property',
  				'post_status' => 'publish',
				'numberposts'      => -1
			));
			$property_leave_date = [];
			foreach($properties_date as $property){
				$leave_dates = json_decode(get_post_meta($property->ID, 'leave_date', true));
				$date_availability = strtotime($_POST['date_availability']) * 1000;
				if($leave_dates){
					foreach($leave_dates as $leave_date){
						if(is_array($leave_date)){
							if($date_availability >= $leave_date[0] && $date_availability <= $leave_date[1]){
								$property_leave_date[] = $property->ID;
								break;
							}
						}
						else if($leave_date == $date_availability){
							$property_leave_date[] = $property->ID;
							break;
						}
					}
				}
			}
			$wp_query->set( 'post__not_in', $property_leave_date );
		}

		if ( ! $meta_query = $wp_query->get( 'meta_query' ) ){
			$meta_query = array('relation' => 'AND');
		}
		if ( isset($_POST['place']) && !empty($_POST['place']) ) {
			$meta_query[] = array(
				'key'	 	=> 'place',
				'value'	  	=> (int)$_POST['place'],
				'type'		=> 'NUMERIC',
				'compare'	=> '>='
			);
		}

		$wp_query->set( 'tax_query', $tax_query );
		$wp_query->set( 'meta_query', $meta_query );

		if ( isset($_POST['address']) && !empty($_POST['address']) ) {
			global $wpdb;
			$tablename = $wpdb->prefix.'coordinate_gps';
			$sql = $wpdb->prepare("SELECT search, lat, lon FROM `$tablename` WHERE search LIKE '".sanitize_text_field( $_POST['address'] )."'");
			$results = $wpdb->get_results( $sql , ARRAY_A );

			if(sizeof($results) > 0){
				foreach($results as $r){
					$lat = $r['lat'];
					$lon = $r['lon'];
				}
			}
			else{
				/*$ch = curl_init();
				$url = "https://api-adresse.data.gouv.fr/search/?q=".str_replace(" ", "+", sanitize_text_field( $_POST['address'] ))."&lat=46.974&lon=-1.2925";
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);	
				$r = json_decode( curl_exec($ch) );
				$lat = $r->features[0]->geometry->coordinates[1];
				$lon = $r->features[0]->geometry->coordinates[0];
				curl_close($ch);*/
				$baseURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
				$addressURL = urlencode(sanitize_text_field( $_POST['address']). ', France') . '&key='.get_field('cle_api_google_geocode', 'option');
				$url = $baseURL . $addressURL;

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);	
				$r = json_decode( curl_exec($ch) );

				$lat = $r->results[0]->geometry->location->lat;
				$lon = $r->results[0]->geometry->location->lng;
				curl_close($ch);

				/*$json = file_get_contents($url);
				$response = json_decode($json);
				$lat = $response->results[0]->geometry->location->lat;
				$lon = $response->results[0]->geometry->location->lng;*/
				
				$sql = $wpdb->prepare("INSERT INTO `$tablename` (`search`, `lat`, `lon`) values ('".sanitize_text_field( $_POST['address'] )."', '$lat', '$lon' ) ");
				$wpdb->query($sql);
			}

			$tablename = $wpdb->prefix.'properties_gps';
			$tablename2 = $wpdb->prefix.'term_relationships';
			$distance = $_POST['distance'] * 1000;
			$point = $lat.' '.$lon ;
			if ( isset($_POST['taxo_type']) && !empty($_POST['taxo_type']) ) {
				$sql = $wpdb->prepare("SELECT post_id, ST_Distance_Sphere(ST_GeomFromText('POINT($point)'), coordinate) AS distance  
					FROM `$tablename` as GPS INNER JOIN `$tablename2` as term ON GPS.post_id = term.object_id
					WHERE term_taxonomy_id = ".sanitize_text_field( $_POST['taxo_type'] )."
					HAVING distance < $distance");
			}else{
				$sql = $wpdb->prepare("SELECT post_id, ST_Distance_Sphere(ST_GeomFromText('POINT($point)'), coordinate) AS distance  FROM `$tablename`  HAVING distance < $distance");
			}
			
			$results = $wpdb->get_results( $sql , ARRAY_A );
			if(sizeof($results) < 5){
				if ( isset($_POST['taxo_type']) && !empty($_POST['taxo_type']) ) {
					$sql = $wpdb->prepare("SELECT post_id, ST_Distance_Sphere(ST_GeomFromText('POINT($point)'), coordinate) AS distance  
						FROM `$tablename` as GPS INNER JOIN `$tablename2` as term ON GPS.post_id = term.object_id
						WHERE term_taxonomy_id = ".sanitize_text_field( $_POST['taxo_type'] )."
						ORDER BY distance LIMIT 5");
				}else{
					$sql = $wpdb->prepare("SELECT post_id, ST_Distance_Sphere(ST_GeomFromText('POINT($point)'), coordinate) AS distance  FROM `$tablename` ORDER BY distance LIMIT 5");
				}
				$results = $wpdb->get_results( $sql , ARRAY_A );
			}
			$ids = array();
			foreach($results as $r){
				if(isset($property_leave_date) && !in_array($r['post_id'], $property_leave_date)){
					$ids[] = $r['post_id'];
				}
				else if(!isset($property_leave_date)){
					$ids[] = $r['post_id'];
				}
			}
			if(sizeof($ids) == 0){
				$ids[] = 0;
			}
			$wp_query->set( 'post__in', $ids );
			
		}
		//var_dump($wp_query);
	}
}
add_action( 'pre_get_posts','ihag_query_property' );


/************* Page option *******************/
if( function_exists('acf_add_options_page') ) {
// Page principale
acf_add_options_page(array(
	'page_title'    => 'Options',
	'menu_title'    => 'Options',
	'menu_slug'     => 'options-generales',
	'capability'    => 'edit_posts',
	'redirect'      => true
));

// Page d'options
acf_add_options_sub_page(array(
  'page_title' 	=> 'Options Générales',
  'menu_slug' 	=> 'acf_options',
  'parent_slug'   => 'options-generales'
));
}



add_action( 'add_meta_boxes_comment', 'pmg_comment_tut_add_meta_box' );
function pmg_comment_tut_add_meta_box()
{
    add_meta_box( 'pmg-comment-title', __( 'Demande d\'informations' ), 'pmg_comment_tut_meta_box_cb', 'comment', 'normal', 'core' );
}

function pmg_comment_tut_meta_box_cb( $comment )
{
    ?>
	<style>
		.postarea#postdiv{display:none}
	</style>
    <p>
        <label for="pmg_comment_title"><?php _e( 'Télephone' ); ?> : </label><?php echo get_comment_meta( $comment->comment_ID, 'phone', true ); ?><br>
		<label for="pmg_comment_title"><?php _e( 'Date' ); ?> : </label><?php echo date_i18n( get_option( 'date_format' ), strtotime( get_comment_meta( $comment->comment_ID, 'ask_date', true ) ) ); ?><br>
		<label for="pmg_comment_title"><?php _e( 'Créneau' ); ?> : </label><?php echo get_comment_meta( $comment->comment_ID, 'ask_time_slot', true ); ?><br>
		<label for="pmg_comment_title"><?php _e( 'Besoin' ); ?> : </label><?php echo get_comment_meta( $comment->comment_ID, 'ask_what', true ); ?><br>
		<label for="pmg_comment_title"><?php _e( 'Participants' ); ?> : </label><?php echo get_comment_meta( $comment->comment_ID, 'ask_participants_number', true ); ?><br>
		<label for="pmg_comment_title"><?php _e( 'Commentaires' ); ?> : </label><?php echo get_comment_meta( $comment->comment_ID, 'ask_comment', true ); ?><br>
    </p>
    <?php
}


// Menu coworker & Menu owner
function register_my_menus() {
	register_nav_menus(
		array(
			'coworker-menu' => __( 'Coworker Menu' ),
			'owner-menu' => __( 'Owner Menu' ),
			'main-loggedIn-owner' => __( 'Menu Header Connecter Owner' ),
			'main-loggedIn' => __( 'Menu Header Connecter Coworker' )
		)
	);
}
add_action( 'init', 'register_my_menus' );


/*add_action( 'wp_dashboard_setup', 'ihag_dashboard_pre_owner' );
function ihag_dashboard_pre_owner() {
	wp_add_dashboard_widget( 'ihag_dashboard_pre_owner', 'Bailleur en attente', 'ihag_dashboard_pre_owner_display' );
}
function ihag_dashboard_pre_owner_display() {
	?>
        <ul>
            <?php
            $users = get_users('role=preowner');
            foreach ( $users as $user ) :
                echo '<li><a href="' . admin_url( 'user-edit.php?user_id=' . $user->ID ) . '">' . $user->first_name.' '.$user->last_name. '</a> <a href="'.admin_url().'?preowner=to_validate&user_id=' . $user->ID.'" class="button">Valider la demande</a></li>';
            endforeach;
            ?>
        </ul>
        <a class="button" href="<?php echo admin_url();?>users.php?role=preowner">Tous les bailleurs en attente</a>
	<?php
}*/

/* add_action('init', function() {
    if(isset($_GET['preowner']) && $_GET['preowner'] == 'to_validate'){
        if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
            $user = new WP_User( $_GET['user_id'] );
            $user->remove_role( 'preowner' );
            $user->add_role( 'owner' );

			$password = wp_generate_password();
			wp_set_password( $password, $user->ID );
            
            $headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
            $to = $user->user_email;
            $subject = "Votre accès AvecOuSansCravate est validée";
            $body = "<p>Félicitations,</p>
			<p>Vous êtes maintenant membre, vous pouvez dès à présent rédiger votre annonce, ajouter vos photos et mettre à jour la disponibilité de vos locaux.</p>
			<p>Vos identifiants sont :<br>identifiant : ".$user->user_login."<br>Mot de passe : ".$password."<br>Adresse de connexion : ".get_permalink( get_field('connect','option') )."</p>
			<p>Cordialement,<br>			
			L'équipe AOSC,</p>";
            wp_mail( $to, $subject, $body, $headers);

        }
    }
});
*/


function my_front_end_login_fail( $username ) {
   $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
      exit;
   }
}
add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login 

function secure_role($role){
	if(is_user_logged_in()){
		$user = wp_get_current_user();
		$user_roles = $user->roles;
		if ( !in_array( $role, $user_roles, true ) && !in_array( 'administrator', $user_roles, true ) ){
			if($role == "owner" && in_array( "coworker", $user_roles, true )){
				wp_redirect( get_permalink( get_field("dashboard-coworker", "option" )), 302);		
			}
			else if($role == "coworker" && in_array( "owner", $user_roles, true )){
				wp_redirect( get_permalink( get_field("dashboard-owner", "option" )), 302);		
			}
			else{
				wp_logout();
				wp_redirect( home_url(), 302);
			}
		}
		return $user;
	}
	else{
		wp_redirect( home_url(), 302);
	}
}

add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );
function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

    // Start with the default content.
   /* $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
    $message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
    $message .= sprintf( __( 'Site Name: %s' ), $site_name ) . "\r\n\r\n";

    $message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
    $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
    $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
    $message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";
*/
    /*
     * If the problem persists with this filter, remove
     * the last line above and use the line below by
     * removing "//" (which comments it out) and hard
     * coding the domain to your site, thus avoiding
     * the network_site_url() function.
     */
    // $message .= '<http://yoursite.com/wp-login.php?action=rp&key=' . $key . '&login=' . rawurlencode( $user_login ) . ">\r\n";

    // Return the filtered message.

	$message = "<p>Cher membre,</p>
	<p>Vous avez demandé la réinitialisation de votre mot de passe.</p>
	<p>Si vous n’êtes pas à l’origine de cette demande, vous pouvez ignorer cet e-mail.</p>
	<p>Pour renouveler votre mot de passe, cliquez sur le lien suivant : <br>";
	$message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">";
	$message .= "</p>";

	$message .= "<p>Pour toutes interogations, nous restons disponible par mail : contact@avecousanscravate.com</p><p>L’équipe AOSC</p>";

    return $message;

}


// disable the admin bar
add_filter('show_admin_bar', '__return_false');


// Modifier le logo sur la page de connexion à l'administration
function wpm_login_style() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png);
			background-size:150px;
			width:150px;
			height:110px;
        }
		
		/*Vous pouvez ajouter d'autres styles CSS ici */
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'wpm_login_style' );


