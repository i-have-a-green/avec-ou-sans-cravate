<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();

?>

<div class="alignwide">
	<div class="property_form">
		<form class="property_search_form" method="post" action="<?php echo get_post_type_archive_link( 'property' ); ?>">
			<div class="inner_form">
				<input type="hidden" name="action" value="search_property">
				<label for="taxo_type"><?php _e("Type d'espace de travail","aosc");?></label>
				<select name="taxo_type" id="taxo_type">
					<option value=""><?php _e("Que cherchez vous ?","aosc");?></option>
					<?php
					$types = get_terms(array('taxonomy' => 'type'));
					foreach($types as $type):?>
						<option value="<?php echo $type->term_id;?>" <?php selected($type->term_id, $_POST['taxo_type']);?>><?php echo $type->name;?></option>
					<?php 
					endforeach;
					?>
				</select>
			</div>
			<div class="inner_form">
				<label for="address"><?php _e("Ville","aosc");?></label>
				<input type="text" name="address" id="address" placeholder="<?php _e("Où cherchez vous ?","aosc");?>" value="<?php echo (isset($_POST['address'])) ? stripslashes($_POST['address']) : '';?>">
			</div>
			<div class="inner_form">
				<label for="distance"><?php _e("Rayon en Km","aosc");?></label>
				<input type="number" min=1 max="100" step="1" name="distance" value="<?php echo (isset($_POST['distance'])) ? $_POST['distance'] : '10';?>" id="distance" placeholder="<?php _e("Distance","aosc");?>">
			</div>
			<div class="inner_form">
				<label for="date_availability"><?php _e("Date","aosc");?></label>
				<input type="date" name="date_availability" id="date_availability" placeholder="<?php _e("Quand ?","aosc");?>" min="<?php echo date("Y-m-d");?>" value="<?php echo (isset($_POST['date_availability'])) ? $_POST['date_availability'] : '';?>">
			</div>
			<div class="inner_form">
				<label for="place"><?php _e("Nombre de place","aosc");?></label>
				<input type="number" min=1 max="100" step="1" name="place" value="<?php echo (isset($_POST['place'])) ? $_POST['place'] : '1';?>" id="place" placeholder="<?php _e("Qui ?","aosc");?>">
			</div>
			<div class="inner_form submit-image">
				<input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/images/Loupe.png" name="clicImage"/>
			</div>
		</form>
	</div>
	<div class="container">
		<?php if (have_posts()) : ?>

		<div class="map_container">
			<div id="mapProperties"></div>
		</div>

		<div class="property_grid">
			<?php
				$whishlistUser = array(); 
				if(is_user_logged_in()){
					$user = wp_get_current_user();
					$whishlistUser = get_user_meta($user->id, 'wishlist', true);
					if(!is_array($whishlistUser)){
						$whishlistUser = array(); 
					}
				}

				while ( have_posts() ) : ?>
				<?php the_post(); ?>
				<?php 
					set_query_var( 'whishlistUser', $whishlistUser );
					get_template_part( 'template-parts/content/content', 'property' ); 
					if(!empty(trim(get_field('gps')))){
						$terms = get_the_terms($post->ID, "type");
						$picto = '';
						if(isset($terms[0])){
							$picto = get_field('picto', 'type_'.$terms[0]->term_id);
						}
						$during = '';
						if(get_field('during') == 'hour'){$during = "L’heure";}
						else if(get_field('during') == 'hours'){$during = "Les deux heures";}
						else if(get_field('during') == 'half-day'){$during = "La demi-journée";}
						else if(get_field('during') == 'day'){$during = "La journée";}
						else if(get_field('during') == 'week'){$during = "La semaine";}

						$tab_properties[] = array(
							'name'		=> '<a href="'.get_permalink().'" class="h4" style="color:#000"><div style="padding:0em;text-align:center;line-height:2">'.get_field('price').'€ '.$during.'</div></a>',
							'link'		=> get_permalink(), 
							'coordonate'=> get_field('gps'),
							'picto'		=> $picto,
						);
					}
					
				?>
			<?php endwhile; ?>
		</div>
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
		<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
		<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
		
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
		<script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>
		<?php
			//affichage de tous les espaces

			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			if(!isset($_POST['action']) && $paged == 1){
				global $post;
				$myposts = get_posts( array(
					'posts_per_page' => -1,
					'post_type'      => 'property',
					'post_status'    => 'publish'
				) );

				if ( $myposts ) {
					$tab_properties = [];
					foreach ( $myposts as $post ) : 
						setup_postdata( $post );
						set_query_var( 'whishlistUser', $whishlistUser );
						if(!empty(trim(get_field('gps')))){
							$terms = get_the_terms($post->ID, "type");
							if(isset($terms[0])){
								$picto = get_field('picto', 'type_'.$terms[0]->term_id);
							}
							$during = '';
							if(get_field('during') == 'hour'){$during = "L’heure";}
							else if(get_field('during') == 'hours'){$during = "Les deux heures";}
							else if(get_field('during') == 'half-day'){$during = "La demi-journée";}
							else if(get_field('during') == 'day'){$during = "La journée";}
							else if(get_field('during') == 'week'){$during = "La semaine";}
	
							$tab_properties[] = array(
								'name'		=> '<a href="'.get_permalink().'" class="h4" style="color:#000"><div style="padding:0em;text-align:center;line-height:2">'.get_field('price').'€ '.$during.'</div></a>',
								'link'		=> get_permalink(), 
								'coordonate'=> get_field('gps'),
								'picto'		=> $picto,
							);
						}
					endforeach;
					wp_reset_postdata();
				}
			}
			global $lon, $lat;
			if(empty($lon)){
				$lon = -1.2925;
			}
			if(empty($lat)){
				$lat = 46.974;
			}

		?>
		<script>
			var lat = <?php echo $lat;?>;
			var lon = <?php echo $lon;?>;
			var macarte = null;
			var zoom = 9;
			var tab_properties = <?php echo json_encode($tab_properties);?>;
			document.addEventListener('DOMContentLoaded', function() {
				if(getCookie('map') == 1){
					mapOpen();
				}
			});
		</script>
		<button class="button mapOpener" onclick="mapOpen()">Voir la carte</button>

	</div>
	
	<div class="alignwide sos-container">
		<p>Vous ne trouvez pas le bien adapté à votre demande?</p>
		<a class="button sos-btn" href="<?php echo get_permalink(get_field('ask-property', 'option'));?>">SOS Recherche</a>
	</div>

	<nav class="blog-pagination">
		<?php the_posts_pagination(); ?>
	</nav>

	<?php else : ?>
		<?php get_template_part( 'template-parts/content/content-none' ); ?>
	<?php endif; ?>

</div>



<?php get_footer(); ?>
