<?php


add_action('admin_menu', 'ihag_user_sub_menu_role');
function ihag_user_sub_menu_role() {
   
    add_submenu_page(
        'tools.php',
        'exportContact',
        'Export CSV Prises de contact',
        'manage_options',
        'exportContact',
        'exportContact',
    );
    global $submenu;
    unset($submenu[ 'users.php' ][16]);
}

function exportContact(){
    ?>
    <div class="wrap">
        <h1>Export des Prises de contact</h1>
        <a href="?report=exportContact" class="button">Export</a>
    <?php
}

add_action('init', function() {
    if(isset($_GET['report']) && $_GET['report'] == 'exportContact'){
        exportCSV();
    }
});

function exportCSV(){
    global $wpdb, $post;
    $csv_fields=array();
    $csv_fields[] = 'Biens';
    $csv_fields[] = 'Nom owner';
    $csv_fields[] = 'Prenom owner';
    $csv_fields[] = 'Coworker';
    $csv_fields[] = 'Date souhaitée';
    $csv_fields[] = 'Participants';

    $output_filename = "aosc_".date("Y-m-d H:i:s").'.csv';
    $output_handle = @fopen( 'php://output', 'w' );
    header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
    header( 'Content-Description: File Transfer' );
    header( 'Content-type: text/csv' );
    header( 'Content-Disposition: attachment; filename=' . $output_filename );
    header( 'Expires: 0' );
    header( 'Pragma: public' );
    // Insert header row
    fputcsv( $output_handle, $csv_fields,";" );
    $args = array( 
        'posts_per_page' => -1, 
        'post_type' => 'property' ,
        'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash') 
    );
    $myposts = get_posts( $args );
    foreach ( $myposts as $post ) : setup_postdata( $post );
        $author_id = $post->post_author;
        $user = get_userdata( $author_id );
		$args = array(
			'post_id' => $post->ID, 
		);
		$comments = get_comments( $args );
		
		if(sizeof($comments) > 0){
			foreach($comments as $comment){
				$tab_data = array( 
					get_the_title($post->ID),
					$user->last_name,
					$user->first_name,
					$comment->comment_author,
					date_i18n( get_option( 'date_format' ), strtotime( get_comment_meta( $comment->comment_ID, 'ask_date', true ) ) ),
					get_comment_meta( $comment->comment_ID, 'ask_participants_number', true )
				);

				fputcsv( $output_handle, $tab_data,";");
			}
		}
		
        
    endforeach;
    fclose( $output_handle );
    exit();
}

